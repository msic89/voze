﻿using Voze.Handler;
using Voze.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.ViewModel
{
   public class WorkerViewModel : BaseViewModel
    {
        private ObservableCollection<Worker> workers;
      

     

        public ObservableCollection<Worker> Workers
        {
            get { return workers; }
            set { workers = value; NotifyPropertyChanged(); }
        }

        public WorkerViewModel()
        {
            setDatas();
        }

        public WorkerViewModel(string name)
        {
            setDatas(name);
        }

        private void setDatas()
        {
            try
            {
                Workers = WorkerHandler.Get();
                if (Workers.Count <= 0)
                    MsgWorker = Application.Current.FindResource("_nofound").ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void setDatas(string name)
        {
            try
            {
                Workers = WorkerHandler.Find(name);
                if (Workers.Count <= 0)
                    MsgWorker = Application.Current.FindResource("_nofound").ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
