﻿using Voze.Handler;
using Voze.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.ViewModel
{
    public class SettingsViewModel : BaseViewModel
    {
        private ObservableCollection<Mark> marks;
        public ObservableCollection<Mark> Marks
        {
            get { return marks; }
            set { marks = value; NotifyPropertyChanged(); }
        }
        private ObservableCollection<Worker> workers;
        public ObservableCollection<Worker> Workers
        {
            get { return workers; }
            set { workers = value; NotifyPropertyChanged(); }
        }
        public SettingsViewModel()
        {
            setDatas();
        }

        private void setDatas()
        {
            Marks = MarkHandler.Get();
            string from = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            string to = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
            Workers = WorkerHandler.GetListToday(from, to);
        }
    }
}
