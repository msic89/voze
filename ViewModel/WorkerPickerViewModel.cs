﻿using Voze.Handler;
using Voze.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.ViewModel
{
   public class WorkerPickerViewModel : BaseViewModel
    {
        private ObservableCollection<Worker> workers;

        public ObservableCollection<Worker> Workers
        {
            get { return workers; }
            set { workers = value; NotifyPropertyChanged(); }
        }



        public WorkerPickerViewModel()
        {
            setDatas();
        }

        public WorkerPickerViewModel(string name)
        {
            setDatas(name);
        }

        private void setDatas()
        {
            try
            {
                Workers = WorkerHandler.Get();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void setDatas(string name)
        {
            try
            {
                Workers = WorkerHandler.Find(name);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


    }
}
