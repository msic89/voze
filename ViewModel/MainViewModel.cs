﻿using Voze.Handler;
using Voze.Helper;
using Voze.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.ViewModel
{
    public class MainViewModel: BaseViewModel
    {
        private ObservableCollection<Option> options;
        private ObservableCollection<Mark> marks;
        private User session;

        public ObservableCollection<Option> Options
        {
            get { return options; }
            set { options = value; NotifyPropertyChanged(); }
        }
        public ObservableCollection<Mark> Marks
        {
            get { return marks; }
            set { marks = value; NotifyPropertyChanged(); }
        }
        public User Session
        {
            get { return session; }
            set { session = value; NotifyPropertyChanged(); }
        }
        public MainViewModel(User user)
        {

            setMenuItems(user);
        }

        private void setMenuItems(User user)
        {

            Session = user;
            Options = Configuration.getMenu();
            Marks =  MarkHandler.Get();
           // GetMarks();

        }

        private async void GetMarks()
        {
            Marks = await MarkHandler.GetAsync();
        }

    }
}
