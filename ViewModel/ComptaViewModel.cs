﻿using Voze.Handler;
using Voze.Helper;
using Voze.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.ViewModel
{
   public class ComptaViewModel : BaseViewModel
    {
        #region properties
        private ObservableCollection<Wash> washList;
        private ObservableCollection<Worker> workers;
        private decimal cumul;
        private decimal lost;
        private string nbCar;
        private string nbTime;
        private string nbWorkers;
        private string nbTimes;

        #endregion

        #region public properties
        public ObservableCollection<Wash> WashList
        {
            get { return washList; }
            set { washList = value; NotifyPropertyChanged(); }
        }

        public decimal Cumul
        {
            get { return cumul; }
            set { cumul = value; NotifyPropertyChanged(); }
        }
        public decimal Lost
        {
            get { return lost; }
            set { lost = value; NotifyPropertyChanged(); }
        }
        public decimal Win
        {
            get { return cumul-lost; }          
        }
        public string NbCar
        {
            get { return nbCar; }
            set { nbCar = value; NotifyPropertyChanged(); }
        }
        public string NbTime
        {
            get { return nbTime; }
            set { nbTime = value; NotifyPropertyChanged(); }
        }
        public string NbTimeS
        {
            get { return nbTimes; }
            set { nbTimes = value; NotifyPropertyChanged(); }
        }
        public string NbWorkers
        {
            get { return nbWorkers; }
            set { nbWorkers = value; NotifyPropertyChanged(); }
        }

        public ObservableCollection<Worker> Workers
        {
            get { return workers; }
            set { workers = value; NotifyPropertyChanged(); }
        }

      
        #endregion


        #region Constructors
        public ComptaViewModel()
        {
            string from = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            string to = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
            setDatas(from, to);
        }
      

        public ComptaViewModel(string from, string to)
        {
            setDatas(from, to);
        }

       

        #endregion



        #region Methods
        private void setDatas(string from, string to)
        {

            try
            {
                Cumul = 0; Lost = 0;
                WashList = WashHandler.Get(from, to);
                NbCar = Formater.toDouble(WashList.Count);
                foreach (var item in WashList) { Cumul += item.Cost; Lost += item.Worker.Bilan.Win; }
                Workers = WorkerHandler.GetListToday(from, to);
                NbWorkers = Formater.toDouble(Workers.Count);
                setTime(WashList);
                if (WashList.Count <= 0)
                    MsgWashing = Application.Current.FindResource("_nofound").ToString();
                if (Workers.Count <= 0)
                    MsgWorker = Application.Current.FindResource("_nofound").ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
       
        private void setTime(ObservableCollection<Wash> _washList)
        {
            if (_washList.Count > 0)
            {

                DateTime f = DateTime.Parse(_washList[0].CreatedAt);
                DateTime end = DateTime.Parse(_washList[_washList.Count - 1].CreatedAt);
                TimeSpan diff = end - f;



                if (diff.Hours > 0)
                {
                    NbTime = diff.ToString("hh"); NbTimeS = "hh";
                }
                else if (diff.Minutes > 0)
                {
                    NbTime = diff.ToString("mm"); NbTimeS = "mm";
                }
                else
                {
                    NbTime = diff.ToString("ss"); NbTimeS = "ss";
                }

            }
        }
        #endregion



    }
}
