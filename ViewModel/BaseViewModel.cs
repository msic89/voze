﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Voze.ViewModel
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private string msgWashing;
        private string msgWorker;
        public string MsgWashing
        {
            get { return msgWashing; }
            set { msgWashing = value; NotifyPropertyChanged(); }
        }
        public string MsgWorker
        {
            get { return msgWorker; }
            set { msgWorker = value; NotifyPropertyChanged(); }
        }
    }
}
