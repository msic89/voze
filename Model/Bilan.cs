﻿namespace Voze.Model
{
    public class Bilan
    {
        public  int Quota { get; set; }
        public int NbWash { get; set; }
        public  decimal Total { get; set; }
        public  decimal Win { get {  return Total - (Total*Quota)/100; } }


    }
}
