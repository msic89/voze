﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Model
{
    public class Service
    {
        public int Rg { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string CreatedAt { get; set; }
        public int MarkId { get; set; }

    }
}
