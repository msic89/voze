﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.Model
{
   public class Attends
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Finished { get; set; }
        public decimal Status { get; set; }
        private string report;
        public string Report {
            get {
                if (Status < 0) return report=Application.Current.FindResource("_working").ToString();
                else if (Status == 0) return report = Application.Current.FindResource("_ok").ToString();
                else return report = Status.ToString();
            }
            set { report = value; } }

    }
}
