﻿using Voze.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Model
{
    public class User
    {
        public int Rg { get; set; }
        public int Id { get; set; }
        public string UId { get; set; }
        public string Uname { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }
        public string CreatedAt { get; set; }
        public string Initial { get { return Formater.LFirst(Uname); } }
    }
}
