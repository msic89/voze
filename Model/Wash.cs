﻿using System.Collections.ObjectModel;

namespace Voze.Model
{
    public  class Wash
    {
        #region attributs
        public int Id { get; set; }
        public int Rg { get; set; }
        public decimal Cost { get; set; }
        public string Mark { get; set; }
        public string Remark { get; set; }
        public string Customer { get; set; }
        public string Mobile { get; set; }
        public int WorkerId { get; set; }
        public string CreatedAt { get; set; }
        public string NumCar { get; set; }
        public bool Status { get; set; }
        public Worker Worker { get; set; }

        public ObservableCollection<Service> Services { get; set; } 
        #endregion

    }
}
