﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Model
{
   public  class Mark
    {
        public int Rg { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string CreatedAt { get; set; }
        public ObservableCollection<Service> Services { get; set; }

    }
}
