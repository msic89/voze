﻿using Voze.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Model
{
    public class Worker
    {
        public int Rg { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        private string lastname;
        private string firstname;
        
        public string Sex { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public int Quota { get; set; }
        public string CreatedAt { get; set; }
        public string Initial { get { return Formater.LFirst(Name); } }
        public Bilan Bilan { get; set; }
       
        public string Lastname { get { return Formater.getSplit(Name, 1); } set { lastname = value; } }
        public string Firstname { get { return Formater.getSplit(Name, 0); } set { firstname = value; } }
        public Attends Attends { get; set; }
        public ObservableCollection<Wash> Washings { get; set; }


    }
}
