﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Helper
{
    public class Formater
    {
        public static string getSplit(string str,int posi=0)
        {
            if(str.Contains(' '))
             return str.Split(' ')[posi].ToString();
            return "";
        }
        public static string UCFirst(string str)
        {
            if (str.Length > 1)
                return str.Substring(0, 1).ToUpper() + str.Substring(1, str.Length - 1);

            return null;
        }

        /// <summary>
        /// Get first letter in string to uppercase
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string LFirst(string str)
        {
            if (str.Length > 1)
                return str.Substring(0, 1).ToUpper();

            return null;
        }
        public static string toDouble(int str)
        {
            if (str >0 && str < 10)
                return "0" + str;
            else
                return str.ToString();
        }


        public static bool IsNumeric(string str)
        {
            int n;
            return int.TryParse(str, out n);
        }

        public static bool IsUInt(string str)
        {
            int n;
            if (int.TryParse(str, out n))
            {
                if (n > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static string GetMD5(string data)
        {
            MD5 md5 = MD5.Create();
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(data));          
            StringBuilder returnValue = new StringBuilder();
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }
            return returnValue.ToString();
        }

        public static int toInt(string s)
        {
            int n;
            if (int.TryParse(s, out n))
                return n;
            return 0;
        }


    }
}
