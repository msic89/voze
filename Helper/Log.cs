﻿using System.Diagnostics;
using System.Security;

namespace Voze.Helper
{
    
    public class Log
    {
      public static void i(string tag, string message)
        {
           Trace.WriteLine(tag + "*************** " + message);
        }
    }
}
