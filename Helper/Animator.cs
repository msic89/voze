﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Voze.Helper
{
    public class Animator
    {
        #region fade

        /// <summary>
        ///  Fade effect 
        /// </summary>
        /// <param name="control">Control to animate</param>
        /// <param name="from">Opcity start</param>
        /// <param name="to">Opcity end</param>
        /// <param name="duration">Duration of animation</param>
        /// <param name="reverse">initialiser l'element</param>
        public static Storyboard Fade(Border control, double from, double to, double duration, bool reverse)
        {

            var a = new DoubleAnimation
            {
                From = from,
                To = to,
                BeginTime = TimeSpan.FromSeconds(0),
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = reverse
            };
            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, control);
            Storyboard.SetTargetProperty(a, new PropertyPath("Opacity"));
            storyboard.Begin();
            return storyboard;
        }
        /// <summary>
        ///  Fade effect 
        /// </summary>
        /// <param name="control">Control to animate</param>
        /// <param name="from">Opcity start</param>
        /// <param name="to">Opcity end</param>
        /// <param name="duration">Duration of animation</param>
        /// <param name="reverse">initialiser l'element</param>
        public static Storyboard Fade(Control control, double from, double to, double duration, bool reverse)
        {

            var a = new DoubleAnimation
            {
                From = from,
                To = to,
                BeginTime = TimeSpan.FromSeconds(0),
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = reverse
            };
            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, control);
            Storyboard.SetTargetProperty(a, new PropertyPath("Opacity"));
            storyboard.Begin();
            return storyboard;
        }



        /// <summary>
        ///  Faire disparaitre le controle en animant son opacité
        /// </summary>
        /// <param name="control">Control to animate</param>
        /// <param name="duration">Duration of animation</param>
        public static Storyboard Hide(Border control, double duration)
        {
            var a = new DoubleAnimation
            {
                From = control.Opacity,
                To = 0.0,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };
            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, control);
            Storyboard.SetTargetProperty(a, new PropertyPath("Opacity"));
            storyboard.Completed += delegate { control.Visibility = Visibility.Collapsed; };
            storyboard.Begin();
            return storyboard;
        }
        /// <summary>
        ///  Faire disparaitre le controle en animant son opacité
        /// </summary>
        /// <param name="control">Control to animate</param>
        /// <param name="duration">Duration of animation</param>
        public static Storyboard Hide(Control control, double duration)
        {
            var a = new DoubleAnimation
            {
                From = control.Opacity,
                To = 0.0,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };
            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, control);
            Storyboard.SetTargetProperty(a, new PropertyPath("Opacity"));
            storyboard.Completed += delegate { control.Visibility = Visibility.Collapsed; };
            storyboard.Begin();
            return storyboard;
        }

        /// <summary>
        ///  Faire apparaitre le controle en animant son opacité
        /// </summary>
        /// <param name="control">Control to animate</param>
        /// <param name="duration">Duration of animation</param>
        public static Storyboard Show(Border control, double duration)
        {
            control.Opacity = 0;
            control.Visibility = Visibility.Visible;
            var a = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };
            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, control);
            Storyboard.SetTargetProperty(a, new PropertyPath("Opacity"));
            //  storyboard.Completed += delegate { control.Visibility = Visibility.Visible; };
            storyboard.Begin();
            return storyboard;
        }
        /// <summary>
        ///  Faire apparaitre le controle en animant son opacité
        /// </summary>
        /// <param name="control">Control to animate</param>
        /// <param name="duration">Duration of animation</param>
        public static Storyboard Show(Control control, double duration)
        {
            control.Opacity = 0;
            control.Visibility = Visibility.Visible;
            var a = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };
            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, control);
            Storyboard.SetTargetProperty(a, new PropertyPath("Opacity"));
            //  storyboard.Completed += delegate { control.Visibility = Visibility.Visible; };
            storyboard.Begin();
            return storyboard;
        }


        #endregion fade

        #region slide


        /// <summary>
        /// Faire un slide de la droite vers la gauche
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="margin">position de decalage</param>
        /// <param name="duration">La durée de l'animation</param>
        /// <param name="revers">action inverse</param>
        /// <returns></returns>
        public static Storyboard SlideToRight(Border control, double margin, double duration, bool revers = false)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            ThicknessAnimation animation = new ThicknessAnimation();

            if (revers)
            {
                animation.From = new Thickness(0, 0, 0, 0);
                animation.To = new Thickness(0, 0, margin, 0);
            }
            else
            {
                animation.From = new Thickness(0, 0, margin, 0);
                animation.To = new Thickness(0, 0, 0, 0);
            }


            animation.Duration = new Duration(TimeSpan.FromSeconds(duration));
            animation.DecelerationRatio = 0.9;
            animation.AutoReverse = false;

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));

            sb.Begin();

            return sb;

        }

        /// <summary>
        /// Faire un slide de la droite vers la droite
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="margin">position de decalage</param>
        /// <param name="duration">La durée de l'animation</param>
        /// <param name="revers">action inverse</param>
        /// <returns></returns>
        public static Storyboard SlideToLeft(Border control, double margin, double duration, bool revers = false)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            ThicknessAnimation animation = new ThicknessAnimation();

            if (revers)
            {
                animation.From = new Thickness(0, 0, 0, 0);
                animation.To = new Thickness(margin, 0, 0, 0);
            }
            else
            {
                animation.From = new Thickness(margin, 0, 0, 0);
                animation.To = new Thickness(0, 0, 0, 0);
            }


            animation.Duration = new Duration(TimeSpan.FromSeconds(duration));
            animation.DecelerationRatio = 0.9;
            animation.AutoReverse = false;

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));

            sb.Begin();

            return sb;

        }

        /// <summary>
        /// Faire un slide de la droite vers le bas
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="margin">position de decalage</param>
        /// <param name="duration">La durée de l'animation</param>
        /// <param name="revers">action inverse</param>
        /// <returns></returns>
        public static Storyboard SlideToTop(Border control, double margin, double duration, bool revers = false)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            ThicknessAnimation animation = new ThicknessAnimation();

            if (revers)
            {
                animation.From = new Thickness(0, 0, 0, 0);
                animation.To = new Thickness(0, 0, 0, margin);
            }
            else
            {
                animation.From = new Thickness(0, 0, 0, margin);
                animation.To = new Thickness(0, 0, 0, 0);
            }


            animation.Duration = new Duration(TimeSpan.FromSeconds(duration));
            animation.DecelerationRatio = 0.9;
            animation.AutoReverse = false;

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));

            sb.Begin();

            return sb;

        }

        /// <summary>
        /// Faire un slide de la droite vers le haut
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="margin">position de decalage</param>
        /// <param name="duration">La durée de l'animation</param>
        /// <param name="revers">action inverse</param>
        /// <returns></returns>
        public static Storyboard SlideToBottom(Border control, double margin, double duration, bool revers = false)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            ThicknessAnimation animation = new ThicknessAnimation();

            if (revers)
            {
                animation.From = new Thickness(0, 0, 0, 0);
                animation.To = new Thickness(0, margin, 0, 0);
            }
            else
            {
                animation.From = new Thickness(0, margin, 0, 0);
                animation.To = new Thickness(0, 0, 0, 0);
            }


            animation.Duration = new Duration(TimeSpan.FromSeconds(duration));
            animation.DecelerationRatio = 0.9;
            animation.AutoReverse = false;

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));

            sb.Begin();

            return sb;

        }



        /// <summary>
        /// reduit ou en l'augmente la largeur 
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="to">largeur finale</param>
        /// <param name="duration">Durée d'animation en seconde</param>
        /// <returns></returns>
        public static Storyboard SlideWidth(Border control, double to, double duration)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            DoubleAnimation animation = new DoubleAnimation()
            {
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Width"));

            sb.Begin();

            return sb;

        }

        /// <summary>
        /// reduit ou en l'augmente la hauteur
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="to">larger finale</param>
        /// <param name="duration">Durée d'animation en seconde</param>
        /// <returns></returns>
        public static Storyboard SlideHeight(Border control, double to, double duration)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            DoubleAnimation animation = new DoubleAnimation()
            {
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("MinHeight"));

            sb.Begin();

            return sb;

        }
        /// <summary>
        /// reduit ou en l'augmente la hauteur
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="to">larger finale</param>
        /// <param name="duration">Durée d'animation en seconde</param>
        /// <returns></returns>
        public static Storyboard SlideHeight(Window control, double to, double duration)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            DoubleAnimation animation = new DoubleAnimation()
            {
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("MinHeight"));

            sb.Begin();

            return sb;

        }
        /// <summary>
        /// reduit ou en l'augmente la hauteur
        /// </summary>
        /// <param name="control">Controle à animer</param>
        /// <param name="from">larger initiale</param>
        /// <param name="to">larger finale</param>
        /// <param name="duration">Durée d'animation en seconde</param>
        /// <returns></returns>
        public static Storyboard SlideHeight(Border control, double from, double to, double duration)
        {

            Storyboard sb = new Storyboard();
            sb.Begin(control);
            DoubleAnimation animation = new DoubleAnimation()
            {
                From = from,
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(duration)),
                AutoReverse = false
            };

            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, control);
            Storyboard.SetTargetProperty(animation, new PropertyPath("MinHeight"));

            sb.Begin();

            return sb;

        }

        #endregion slide

        #region model
        //public static void slideTo(Border element, double margin, double sec, DependencyProperty property, bool revers=false)
        //{
        //    ThicknessAnimation animation = new ThicknessAnimation();
        //    if (revers)
        //    {
        //        animation.From = new Thickness(0, 0, 0, 0);
        //        animation.To = new Thickness(margin, 0, 0, 0);
        //    }
        //    else
        //    {
        //        animation.From = new Thickness(margin, 0, 0, 0);
        //        animation.To = new Thickness(0, 0, 0, 0);
        //    }
        //    animation.Duration = new Duration(TimeSpan.FromSeconds(sec));
        //    animation.AutoReverse = false;
        //    element.BeginAnimation(property, animation);
        //}
        #endregion model

    }
}
