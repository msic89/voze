﻿using Voze.Model;
using Voze.View;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;


namespace Voze.Helper
{
    public class Configuration
    {

        #region Languages
        public static void SetLanguage()
        {
            ResourceDictionary dict = new ResourceDictionary();
            switch (Thread.CurrentThread.CurrentCulture.ToString())
            {
                case "ru-RU":
                    dict.Source = new Uri("/Voze;component/Resources/Lang/ru/string.xaml", UriKind.Relative);
                    break;
                case "fr-FR":
                    dict.Source = new Uri("/Voze;component/Resources/Lang/fr/string.xaml", UriKind.Relative);
                    break;
                default:
                    dict.Source = new Uri("/Voze;component/Resources/Lang/string.xaml", UriKind.Relative);
                    break;
            }
            Application.Current.Resources.MergedDictionaries.Add(dict);
        }

        public static void SetLanguage(string lang)
        {
            ResourceDictionary dict = new ResourceDictionary();
            switch (lang)
            {
                case "ru-RU" :
                case "0":
                    dict.Source = new Uri("/Voze;component/Resources/Lang/ru/string.xaml", UriKind.Relative);

                    break;
                case "fr-FR":
                case "1":
                    dict.Source = new Uri("/Voze;component/Resources/Lang/fr/string.xaml", UriKind.Relative);
                    break;
                case "en-US":
                case "2":
                    dict.Source = new Uri("/Voze;component/Resources/Lang/string.xaml", UriKind.Relative);
                    break;
                default:
                    dict.Source = new Uri("/Voze;component/Resources/Lang/string.xaml", UriKind.Relative);
                    break;
            }
            Application.Current.Resources.MergedDictionaries.Add(dict);
        }

        public static string getCode()
        {
            DateTime now = DateTime.Now;
            return now.ToString("yy") + now.ToString("HH") + now.ToString("dd") + now.ToString("mm") +
                now.ToString("MM") + now.ToString("ss");
        }

        public static async Task SetLanguageAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);
             SetLanguage();
        }
        public static async Task SetLanguageAsync(string lang)
        {
            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);
            SetLanguage(lang);
        }

        public static ObservableCollection<Option> getMenu()
        {
           return  new ObservableCollection<Option>(){

                    new Option(){ Title=Application.Current.FindResource("_dashboard").ToString(), Icon="Resources\\Img\\ic_dashboard.png" },
                    new Option(){ Title=Application.Current.FindResource("_employees").ToString(), Icon="Resources\\Img\\ic_users.png" },
                     new Option(){ Title=Application.Current.FindResource("_accounting").ToString(), Icon="Resources\\Img\\ic_compta.png" },

                    new Option(){ Title=Application.Current.FindResource("_system").ToString(), Icon="Resources\\Img\\ic_settings.png" },
                   // new Option(){ Title=Application.Current.FindResource("_options").ToString(), Icon="Resources\\Img\\ic_settings.png" },
                    //new Option(){ Title=Application.Current.FindResource("_profile").ToString(), Icon="Resources\\Img\\admin_user.png" },

                   
                 };
        }


        public static int Save(int pos)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update configuration SET lang=@1 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", 1);
                    cmd.Parameters.AddWithValue("1", pos);                  
                    result += cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> SaveAsync(int pos)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Save(pos);

            return result;
        }
        public static int Get()
        {
            int result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT lang FROM configuration where id=@0", cnx))
                {
                    cmd.Parameters.AddWithValue("0", 1);
                    SQLiteDataReader rd = cmd.ExecuteReader();

                    if (rd.Read())
                    {
                        result = rd.GetInt32(0);
                    }
                    rd.Close();

                }

                cnx.Close();
            }
            return result;
        }
        public static int GetSession()
        {
            int result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT keepsession FROM configuration where id=@0", cnx))
                {
                    cmd.Parameters.AddWithValue("0", 1);
                    SQLiteDataReader rd = cmd.ExecuteReader();

                    if (rd.Read())
                    {
                        result = rd.GetInt32(0);
                    }
                    rd.Close();

                }

                cnx.Close();
            }
            return result;
        }
        public static int SetSession(int pos)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update configuration SET keepsession=@1 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", 1);
                    cmd.Parameters.AddWithValue("1", pos);
                    result += cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cnx.Close();
            }

            return result;
        }

        #endregion

        #region backup
        private static void SaveFile(string source = "voze.db", string destination = @"backup\voze.db", bool callback = false)
        {
            FileInfo fi = new FileInfo(source);
            if (fi.Exists)
            {
                try
                {
                    fi.CopyTo(destination, true);
                    if (callback)
                        MsBoxView.Show("File saved succesfully!");
                }
                catch (Exception ex)
                {
                    MsBoxView.Show(ex.Message);
                }

            }

            else
                MsBoxView.Show("File does not exist");
        }

        /// <summary>
        /// Save database to 
        /// </summary>
        /// <param name="dir">Can change directory</param>
        /// <param name="callback">Callback after saving</param>
        public static void Backup(bool dir = false, bool callback = false)
        {
            if (dir)
            {
                string sourceFile = "voze.db";
                string destinationFile = @"C:\voze.db";

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                sfd.RestoreDirectory = true;

                sfd.Filter = "Database file(*.db)|*.db";
                sfd.Title = "Save database";
                if (sfd.ShowDialog() == true)
                {
                    destinationFile = sfd.FileName;
                    SaveFile(sourceFile, destinationFile, true);
                }
            }
            else
            {
                SaveFile();

            }
        }
        #endregion

    }
}
