﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Helper
{
    public class Periode
    {
        public static int ActualYear()
        {
           return DateTime.Now.Year;
        }

        public static DateTime LastWeekFromToday()
        {
            return DateTime.Now.AddDays(-7);
        }
        public static DateTime FirstDateOfCurrentMonth()
        {
            var now = DateTime.Now;
            return new DateTime(now.Year, now.Month, 1);
        }
        public static DateTime LastDateOfCurrentMonth()
        {
            var now = DateTime.Now;
            var DaysInMonth = DateTime.DaysInMonth(now.Year, now.Month);
            return new DateTime(now.Year, now.Month, DaysInMonth);
        }
        public static DateTime FirstDateOfCurrentYear()
        {
            var now = DateTime.Now;
            return new DateTime(now.Year, 1, 1);
        }
        public static DateTime LastDateOfCurrentYear()
        {
            var now = DateTime.Now;
            var DaysInMonth = DateTime.DaysInMonth(now.Year, 12);
            return new DateTime(now.Year, 12, 31);
        }


    }
}
