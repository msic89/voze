﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Voze.Helper
{
   public class Generator
    {
        private static readonly Random rand = new Random();
        private static readonly string[] colors = new string[] {
           "#00a300","#b91d47","#27a8e1","#ff0097","#7e3878","#2b5797","#45b6e0","#01c0c8","#6ac5e6","#ef6088","#f280a0","#57c386","#79cf9e","#00c292","#7c79b6","#9694c5","#f7fafc",
        };



        public static Brush Colors()
        {
          
            int b = rand.Next(0, colors.Length);

            return (Brush)new BrushConverter().ConvertFromString(colors[b]);
        }

    }
}
