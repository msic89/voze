﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Helper
{
    public enum MsBoxImage
    {
        Warning = 0,
        Question,
        Information,
        Error,
        None
    }
}
