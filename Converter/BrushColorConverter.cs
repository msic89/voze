﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Voze.Converter
{
    class BrushColorConverter : IValueConverter
    {

        public object Convert(object value, Type targetType,
        object parameter, CultureInfo culture)
        {
            if (value is Color)
            {
                return new SolidColorBrush((Color)value);

            }
            return new BrushConverter().ConvertFromString(value.ToString()) as SolidColorBrush;
        }

        public object ConvertBack(object value, Type targetType,
              object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

}

