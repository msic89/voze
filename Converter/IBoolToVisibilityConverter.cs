﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Voze.Converter
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class IBoolToVisibilityConverter : IValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public IBoolToVisibilityConverter()
        {
            // set defaults
            TrueValue = Visibility.Visible;
            FalseValue = Visibility.Collapsed;
        }

        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return null;
            return (bool)value ? FalseValue :  TrueValue;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (Equals(value, TrueValue))
                return false;
            if (Equals(value, FalseValue))
                return true;
            return null;
        }
    }
}
