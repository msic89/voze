﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Voze.Converter
{
    class StatusColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //bool flag;
            //if (Boolean.TryParse(value.ToString(), out flag))
            //{

            //}
            //else
            //{
                int v = System.Convert.ToInt32(value);
                if (v == -1)
                    return (Brush)new BrushConverter().ConvertFromString("#45b6e0");
                else if (v == 0)
                    return (Brush)new BrushConverter().ConvertFromString("#57c386");
                else
                    return (Brush)new BrushConverter().ConvertFromString("#ef6088");
            //}
                       
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }

    }
}
