﻿using Voze.Helper;
using Voze.Model;
using System.Data.SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.Handler
{
    public class MarkHandler
    {
       
        public static ObservableCollection<Mark> Get()
        {
            var result = new ObservableCollection<Mark>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM mark;", cnx))
                {
                  
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Mark()
                            {
                                Id = rd.GetInt32(0),
                                Title = rd.GetValue(1).ToString(),                              
                                CreatedAt = rd.GetValue(2).ToString(),
                                Services = ServiceHandler.Get(rd.GetInt32(0))
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static async Task<ObservableCollection<Mark>>  GetAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get();

            return result;
        }

      

        public static int Add(Mark mark)
        {
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                int result = 0; string id = "";

                string q1 = @"INSERT INTO mark(Title) VALUES(@0);SELECT LAST_INSERT_ROWID(); ";
                string q2 = @"INSERT INTO service(`Title`, `price`, `mark_id`) VALUES(@0 , @1 , @2);";

                cnx.Open();
                using (SQLiteTransaction trans = cnx.BeginTransaction())
                {
                    try
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(q1, cnx, trans))
                        {
                            cmd.Parameters.AddWithValue("0", mark.Title);
                            id = cmd.ExecuteScalar().ToString();
                            cmd.Parameters.Clear();
                        }

                        if (mark.Services.Count > 0)
                        {
                            foreach (var item in mark.Services)
                            {
                                using (SQLiteCommand cmd = new SQLiteCommand(q2, cnx))
                                {
                                    cmd.Parameters.AddWithValue("0", item.Title);
                                    cmd.Parameters.AddWithValue("1", item.Price);
                                    cmd.Parameters.AddWithValue("2", id);

                                    result = cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.ToString());
                    }


                }

                cnx.Close();


                return result;
            }
        }      
        public static async Task<int> AddAsync(Mark mark)
        {

            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Add(mark);

            return result;
        }
        public static int Insert(string mark)
        {
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                int result = 0;
                string q = @"INSERT INTO mark(Title) VALUES(@0); ";

                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand(q, cnx))
                {
                    cmd.Parameters.AddWithValue("0", mark);
                    result = cmd.ExecuteNonQuery();
                }
                cnx.Close();


                return result;
            }
        }
        public static int Remove(Mark mark)
        {

            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("DELETE FROM mark WHERE Id=@id;", cnx))
                {
                    cmd.Parameters.AddWithValue("id", mark.Id);
                    result = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> RemoveAsync(Mark mark)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Remove(mark);

            return result;
        }

    }
}
