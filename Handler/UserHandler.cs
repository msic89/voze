﻿using Voze.Helper;
using Voze.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;

namespace Voze.Handler
{
    public class UserHandler
    {
        public static User Get(string uid, string pwd)
        {
            User result = null;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM user where uid=@0 and pwd=@1;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", uid);
                    cmd.Parameters.AddWithValue("1", Formater.GetMD5(pwd));
                    SQLiteDataReader rd = cmd.ExecuteReader();
                  
                        if (rd.Read())
                        {
                            result=new User()
                            {
                                Id = rd.GetInt32(0),
                                UId = rd.GetValue(1).ToString(),
                                Pwd = rd.GetValue(2).ToString(),
                                Uname = rd.GetValue(3).ToString(),
                                Email = rd.GetValue(4).ToString(),                            
                            };
                           
                        }
                        rd.Close();
                   
                }

                cnx.Close();
            }
            return result;
        }
        public static async Task<User> GetAsync(string uid, string pwd)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get(uid,pwd);
            return result;
        }

        public static User Get(int id)
        {
            User result = null;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM user where id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", id);
                    SQLiteDataReader rd = cmd.ExecuteReader();

                    if (rd.Read())
                    {
                        result = new User()
                        {
                            Id = rd.GetInt32(0),
                            UId = rd.GetValue(1).ToString(),
                            Pwd = rd.GetValue(2).ToString(),
                            Uname = rd.GetValue(3).ToString(),
                            Email = rd.GetValue(4).ToString(),
                        };

                    }
                    rd.Close();

                }

                cnx.Close();
            }
            return result;
        }
        public static async Task<User> GetAsync(int id)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get(id);
            return result;
        }

        public static int Update(User user)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update user SET nom=@1,email=@2 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", user.Id);
                    cmd.Parameters.AddWithValue("1", user.Uname);
                    cmd.Parameters.AddWithValue("2", user.Email);
                    result += cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cnx.Close();
            }

            return result;
        }
        public static int UpdateWithPassword(User user)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update user SET nom=@1,email=@2,pwd=@3 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", user.Id);
                    cmd.Parameters.AddWithValue("1", user.Uname);
                    cmd.Parameters.AddWithValue("2", user.Email);
                    cmd.Parameters.AddWithValue("3", Formater.GetMD5(user.Pwd));
                    result += cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cnx.Close();
            }

            return result;
        }
    }
}
