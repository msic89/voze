﻿using Voze.Helper;
using Voze.Model;
using System.Data.SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Handler
{
    public class WorkerHandler
    {
        public static ObservableCollection<Worker> Get()
        {
            var result = new ObservableCollection<Worker>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM worker ORDER BY id;", cnx))
                {

                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Worker()
                            {
                                Id = rd.GetInt32(0),
                                Name = rd.GetValue(1).ToString(),
                                Sex = rd.GetValue(2).ToString(),
                                Tel = rd.GetValue(3).ToString(),
                                Address = rd.GetValue(4).ToString(),
                                Quota = rd.GetInt32(5),
                                CreatedAt = rd.GetValue(6).ToString(),
                                Rg = rg,
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static async Task<ObservableCollection<Worker>> GetAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get();

            return result;
        }

        public static ObservableCollection<Worker> Find(string name)
        {
            var result = new ObservableCollection<Worker>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM worker WHERE nom LIKE '" + name + "%' ORDER BY nom; ", cnx))
                {

                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Worker()
                            {
                                Id = rd.GetInt32(0),
                                Name = rd.GetValue(1).ToString(),
                                Sex = rd.GetValue(2).ToString(),
                                Tel = rd.GetValue(3).ToString(),
                                Address = rd.GetValue(4).ToString(),
                                Quota = rd.GetInt32(5),
                                CreatedAt = rd.GetValue(6).ToString(),
                                Rg = rg,
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static async Task<ObservableCollection<Worker>> FindAsync(string name)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Find(name);

            return result;
        }

        public static ObservableCollection<Worker> GetListToday(string from, string to)
        {
            var result = new ObservableCollection<Worker>();
          //  string q = "SELECT * FROM  worker as w ON wh.worker_id=w.id INNER JOIN point as p ON w.id = p.worker_id WHERE p.created_at BETWEEN @1 AND @2; ";
            string q2 = @"SELECT 
 w.id ,w.nom ,w.sex ,w.tel ,w.address ,w.quota ,w.created_at 
,(SELECT  COUNT(wash.id) FROM  wash   WHERE wash.worker_id  = w.id AND wash.created_at BETWEEN @1 AND @2) as nbwash
 ,(SELECT  COALESCE(SUM(wash.cost),0) FROM wash  WHERE wash.worker_id  = w.id   AND wash.created_at BETWEEN @1 AND @2) as sumwash
,p.id ,p.created_at,p.ended_at, p.status  
FROM worker as w  INNER JOIN point as p ON w.id = p.worker_id WHERE p.created_at BETWEEN @1 AND @2 GROUP BY w.id ORDER BY w.nom; 
";
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(q2, cnx))
                {
                    cmd.Parameters.AddWithValue("1", from);
                    cmd.Parameters.AddWithValue("2", to);
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                           
                            result.Add(new Worker()
                            {
                                Id = rd.GetInt32(0),
                                Name = rd.GetValue(1).ToString(),
                                Sex = rd.GetValue(2).ToString(),
                                Tel = rd.GetValue(3).ToString(),
                                Address = rd.GetValue(4).ToString(),
                                Quota = rd.GetInt32(5),
                                CreatedAt = rd.GetValue(6).ToString(),
                                Rg = rg,
                                Bilan = new Bilan {
                                    NbWash = rd.GetValue(7) != null ? rd.GetInt32(7) : 0,
                                    Quota = rd.GetInt32(5),
                                    Total = rd.GetValue(8) != null ? rd.GetDecimal(8) : 0,
                                },
                                Attends = new Attends {
                                    Id = rd.GetInt32(9),
                                    Date = rd.GetValue(10).ToString(),
                                    Finished = rd.GetValue(11).ToString(),
                                    Status = rd.GetDecimal(12),
                                },Washings = WashHandler.Get(from, to, rd.GetInt32(0))
                        });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static async Task<ObservableCollection<Worker>> GetListTodayAsync(string from, string to)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = GetListToday(from,to);

            return result;
        }


        public static bool onList(int id)
        {
            bool rs = false;
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT count(*) FROM point as p WHERE p.worker_id = @0 AND p.created_at BETWEEN @1 AND @2;", cnx))
                {
                    //" presence as p ON e.uniqid = p.employeid WHERE p.created_at BETWEEN @1 AND @2
                    cmd.Parameters.AddWithValue("0", id);
                    cmd.Parameters.AddWithValue("1", DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
                    cmd.Parameters.AddWithValue("2", DateTime.Now.ToString("yyyy-MM-dd 23:59:59"));

                    int c = Convert.ToInt32(cmd.ExecuteScalar());
                    if (c > 0)
                        rs = true;
                }

                cnx.Close();
            }

            return rs;
        }
        public static async Task<bool> onListAsync(int id)
        {

            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = onList(id);

            return result;
        }

        public static int Add(Worker worker)
        {

            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("INSERT INTO worker(nom,sex,tel,address,quota) VALUES(@0,@1,@2,@3,@4);", cnx))
                {
                    cmd.Parameters.AddWithValue("0", worker.Name);
                    cmd.Parameters.AddWithValue("1", worker.Sex);
                    cmd.Parameters.AddWithValue("2", worker.Tel);
                    cmd.Parameters.AddWithValue("3", worker.Address);
                    cmd.Parameters.AddWithValue("4", worker.Quota);

                    result = cmd.ExecuteNonQuery();
                }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> AddAsync(Worker worker)
        {

            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Add(worker);

            return result;
        }

        public static int AddToList(Worker worker)
        {

            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("INSERT INTO point(worker_id) VALUES(@0);", cnx))
                {
                    cmd.Parameters.AddWithValue("0", worker.Id);

                    result = cmd.ExecuteNonQuery();
                }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> AddToListAsync(Worker worker)
        {

            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = AddToList(worker);

            return result;
        }

        public static int Remove(Worker worker)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
               
                    using (SQLiteCommand cmd = new SQLiteCommand("DELETE FROM worker WHERE Id=@0;", cnx))
                    {
                        cmd.Parameters.AddWithValue("0", worker.Id);
                        result += cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
               
                cnx.Close();
            }

            return result;
        }
        public static async Task<int> RemoveAsync(Worker worker)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Remove(worker);

            return result;
        }

        public static int Update(Worker worker)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update worker SET nom=@1,sex=@2,tel=@3,address=@4,quota=@5 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", worker.Id);
                    cmd.Parameters.AddWithValue("1", worker.Name);
                    cmd.Parameters.AddWithValue("2", worker.Sex);
                    cmd.Parameters.AddWithValue("3", worker.Tel);
                    cmd.Parameters.AddWithValue("4", worker.Address);
                    cmd.Parameters.AddWithValue("5", worker.Quota);
                    result += cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> UpdateAsync(Worker worker)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Update(worker);

            return result;
        }


        public static int AfterWork(Attends attends)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update point SET ended_at=@1,status=@2  WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", attends.Id);
                    cmd.Parameters.AddWithValue("1", attends.Finished);
                    cmd.Parameters.AddWithValue("2", attends.Status);
                   
                    result += cmd.ExecuteNonQuery();
                                   }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> AfterWorkAsync(Attends attends)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = AfterWork(attends);

            return result;
        }
        public static int SetStatus(Attends attends)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();

                using (SQLiteCommand cmd = new SQLiteCommand("Update point SET status=@1 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", attends.Id);
                    cmd.Parameters.AddWithValue("1", attends.Status);

                    result += cmd.ExecuteNonQuery();
                }

                cnx.Close();
            }

            return result;
        }
        public static async Task<int> SetStatusAsync(Attends attends)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = SetStatus(attends);

            return result;
        }
    }
}
