﻿using Voze.Helper;
using Voze.Model;
using Voze.View;
using System;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Threading.Tasks;
using System.Windows;

namespace Voze.Handler
{
    public class WashHandler
    {

        #region methods
        public static ObservableCollection<Wash> Get()
        {
            var result = new ObservableCollection<Wash>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM wash as wh INNER JOIN worker w ON wh.worker_id=w.id ;", cnx))
                {

                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Wash()
                            {
                                Rg = rg,
                                Id = rd.GetInt32(0),
                                Cost = rd.GetDecimal(1),
                                Customer = rd.GetValue(2).ToString(),
                                Mobile = rd.GetValue(3).ToString(),
                                Mark = rd.GetValue(4).ToString(),
                                Remark = rd.GetValue(5).ToString(),
                                CreatedAt = rd.GetValue(6).ToString(),
                                WorkerId = rd.GetInt32(7),
                                NumCar = rd.GetValue(8).ToString(),
                                Status = rd.GetBoolean(9),

                                Services = GetServices(rd.GetInt32(0)),
                               
                                Worker = new Worker
                                {
                                    Id = rd.GetInt32(10),
                                    Name = rd.GetValue(11).ToString(),
                                    Sex = rd.GetValue(12).ToString(),
                                    Tel = rd.GetValue(13).ToString(),
                                    Address = rd.GetValue(14).ToString(),
                                    Quota = rd.GetInt32(15),
                                    CreatedAt = rd.GetValue(16).ToString(),
                                }
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }

        public static ObservableCollection<Wash> Get(string from, string to)
        {
            var result = new ObservableCollection<Wash>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM wash as wh INNER JOIN worker w ON wh.worker_id=w.id WHERE wh.created_at BETWEEN @0 AND @1;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", from);
                    cmd.Parameters.AddWithValue("1", to);
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Wash()
                            {
                                Rg = rg,
                                Id = rd.GetInt32(0),
                                Cost = rd.GetDecimal(1),
                                Customer = rd.GetValue(2).ToString(),
                                Mobile = rd.GetValue(3).ToString(),
                                Mark = rd.GetValue(4).ToString(),
                                Remark = rd.GetValue(5).ToString(),
                                CreatedAt = rd.GetValue(6).ToString(),
                                WorkerId = rd.GetInt32(7),
                                NumCar = rd.GetValue(8).ToString(),
                                Status = rd.GetBoolean(9),

                                Services = GetServices(rd.GetInt32(0)),
                                
                                Worker = new Worker
                                {
                                    Id = rd.GetInt32(10),
                                    Name = rd.GetValue(11).ToString(),
                                    Sex = rd.GetValue(12).ToString(),
                                    Tel = rd.GetValue(13).ToString(),
                                    Address = rd.GetValue(14).ToString(),
                                    Quota = rd.GetInt32(15),
                                    CreatedAt = rd.GetValue(16).ToString(),
                                    Bilan = new Bilan
                                    {
                                        Quota = rd.GetInt32(15),
                                        Total = rd.GetDecimal(1)
                                    }
                                }
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static ObservableCollection<Wash> Get(string from, string to, Worker worker)
        {
            var result = new ObservableCollection<Wash>();
          
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM wash WHERE created_at BETWEEN @0 AND @1 AND worker_id=@2;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", from);
                    cmd.Parameters.AddWithValue("1", to);
                    cmd.Parameters.AddWithValue("2", worker.Id);
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Wash()
                            {
                                Rg = rg,
                                Id = rd.GetInt32(0),
                                Cost = rd.GetDecimal(1),
                                Customer = rd.GetValue(2).ToString(),
                                Mobile = rd.GetValue(3).ToString(),
                                Mark = rd.GetValue(4).ToString(),
                                Remark = rd.GetValue(5).ToString(),
                                CreatedAt = rd.GetValue(6).ToString(),
                                WorkerId = rd.GetInt32(7),
                                NumCar = rd.GetValue(8).ToString(),
                                Status = rd.GetBoolean(9),
                                Services = GetServices(rd.GetInt32(0)),
                                
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
               
            }

            return result;
           
        }
        public static ObservableCollection<Wash> Get(string from, string to, int worker)
        {
            var result = new ObservableCollection<Wash>();
          
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM wash WHERE created_at BETWEEN @0 AND @1 AND worker_id=@2;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", from);
                    cmd.Parameters.AddWithValue("1", to);
                    cmd.Parameters.AddWithValue("2", worker);
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Wash()
                            {
                                Rg = rg,
                                Id = rd.GetInt32(0),
                                Cost = rd.GetDecimal(1),
                                Customer = rd.GetValue(2).ToString(),
                                Mobile = rd.GetValue(3).ToString(),
                                Mark = rd.GetValue(4).ToString(),
                                Remark = rd.GetValue(5).ToString(),
                                CreatedAt = rd.GetValue(6).ToString(),
                                WorkerId = rd.GetInt32(7),
                                NumCar = rd.GetValue(8).ToString(),
                                Status = rd.GetBoolean(9),
                                Services = GetServices(rd.GetInt32(0)),
                                
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
               
            }

            return result;
           
        }
        public static ObservableCollection<Service> GetServices(int id)
        {
            var result = new ObservableCollection<Service>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT s.id, s.Title,s.price FROM `wash` w INNER JOIN wash_services ws  ON w.id =ws.wash_id INNER JOIN service s ON ws.service_id = s.id WHERE w.id = @0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", id);
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Service()
                            {
                                Rg = rg,
                                Id = rd.GetInt32(0),
                                Title = rd.GetValue(1).ToString(),
                                Price = rd.GetDecimal(2),
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static int Add(Wash wash)
        {
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                int result = 0; string id = "";

                string q1 = @"INSERT INTO wash(cost, mark, customer, mobile,  worker_id, numcar) VALUES (@0, @1, @2, @3, @4, @5);SELECT LAST_INSERT_ROWID(); ";
                string q2 = @"INSERT INTO wash_services(wash_id,service_id) VALUES(@0, @1);";

                cnx.Open();
                using (SQLiteTransaction trans = cnx.BeginTransaction())
                {
                    try
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(q1, cnx, trans))
                        {
                            cmd.Parameters.AddWithValue("0", wash.Cost);
                            cmd.Parameters.AddWithValue("1", wash.Mark);
                            cmd.Parameters.AddWithValue("2", wash.Customer);
                            cmd.Parameters.AddWithValue("3", wash.Mobile);
                            cmd.Parameters.AddWithValue("4", wash.WorkerId);
                            cmd.Parameters.AddWithValue("5", wash.NumCar);
                            id = cmd.ExecuteScalar().ToString();
                            cmd.Parameters.Clear();
                        }

                        if (wash.Services.Count > 0)
                        {
                            foreach (var item in wash.Services)
                            {
                                using (SQLiteCommand cmd = new SQLiteCommand { Connection = cnx, CommandText = q2, Transaction = trans })
                                {
                                    cmd.Parameters.AddWithValue("@0", id);
                                    cmd.Parameters.AddWithValue("@1", item.Id);
                                    int c1 = cmd.ExecuteNonQuery();
                                    result += c1;

                                }
                            }
                        }
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.ToString());
                    }


                }

                cnx.Close();


                return result;
            }
        }

        public static int Update(Wash wash)
        {
            var result = 0;
            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("Update wash SET status=@1,remark=@2 WHERE Id=@0;", cnx))
                {
                    cmd.Parameters.AddWithValue("0", wash.Id);
                    cmd.Parameters.AddWithValue("1", wash.Status);
                    cmd.Parameters.AddWithValue("2", wash.Remark);
                    result += cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                cnx.Close();
            }

            return result;
        }
        public static async Task<int> UpdateAsync(Wash wash)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Update(wash);

            return result;
        }
        #endregion

        #region Async methods
        public static async Task<ObservableCollection<Wash>> GetAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);
            var result = Get();

            return result;
        }
        public static async Task<ObservableCollection<Wash>> GetAsync(string from, string to)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get(from, to);

            return result;
        }
        public static async Task<ObservableCollection<Wash>> GetAsync(string from, string to, Worker worker)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get(from, to, worker);

            return result;
        }
        public static async Task<int> AddAsync(Wash wash)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            int r = Add(wash);

            return r;
        }
        #endregion



    }
}
