﻿using Voze.Helper;
using Voze.Model;
using System.Data.SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voze.Handler
{
    public  class ServiceHandler
    {
      
        public static ObservableCollection<Service> Get()
        {
            var result = new ObservableCollection<Service>();

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM service;", cnx))
                {
                   
                    SQLiteDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        int rg = 1;
                        while (rd.Read())
                        {
                            result.Add(new Service()
                            {
                                Id = rd.GetInt32(0),
                                Title = rd.GetValue(1).ToString(),
                                Price = rd.GetDecimal(2),
                                MarkId = rd.GetInt32(3),
                                CreatedAt = rd.GetValue(4).ToString(),
                            });
                            rg++;
                        }
                        rd.Close();
                    }
                }

                cnx.Close();
            }


            return result;
        }
        public static async Task<ObservableCollection<Service>> GetAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get();

            return result;
        }

        public static ObservableCollection<Service> Get(int mark)
        {
            var result = new ObservableCollection<Service>();
           
                using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
                {
                    cnx.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM service where mark_id=@0;", cnx))
                    {
                        cmd.Parameters.AddWithValue("0", mark);
                        SQLiteDataReader rd = cmd.ExecuteReader();
                        if (rd.HasRows)
                        {
                            int rg = 1;
                            while (rd.Read())
                            {
                                result.Add(new Service()
                                {
                                    Id = rd.GetInt32(0),
                                    Title = rd.GetValue(1).ToString(),
                                    Price = rd.GetDecimal(2),
                                    MarkId =rd.GetInt32(3),
                                    CreatedAt = rd.GetValue(4).ToString(),
                                });
                                rg++;
                            }
                            rd.Close();
                        }
                    }

                    cnx.Close();
                }
          

            return result;
        }
        public static async Task<ObservableCollection<Service>> GetAsync(int mark)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Get(mark);
            return result;
        }


        public static int Add(ObservableCollection<Service> services)
        {

            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                foreach (var service in services)
                {
                    using (SQLiteCommand cmd = new SQLiteCommand("INSERT INTO service(`Title`, `price`, `mark_id`) VALUES(@0 , @1 , @2);", cnx))
                    {
                        cmd.Parameters.AddWithValue("0", service.Title);
                        cmd.Parameters.AddWithValue("1", service.Price);
                        cmd.Parameters.AddWithValue("2", service.MarkId);

                        result += cmd.ExecuteNonQuery();
                    }
                }


                cnx.Close();
            }

            return result;
        }
        public static async Task<int> AddAsync(ObservableCollection<Service> services)
        {

            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Add(services);

            return result;
        }
        public static int Add(Service service)
        {

            var result = 0;

                using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
                {
                    cnx.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand("INSERT INTO service(`Title`, `price`, `mark_id`) VALUES(@0 , @1 , @2);", cnx))
                    {
                        cmd.Parameters.AddWithValue("0", service.Title);
                        cmd.Parameters.AddWithValue("1", service.Price);
                        cmd.Parameters.AddWithValue("2", service.MarkId);

                        result = cmd.ExecuteNonQuery();
                    }

                    cnx.Close();
                }
           
            return result;
        }
        public static async Task<int> AddAsync(Service service)
        {

            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Add(service);

            return result;
        }

        public static int Remove(ObservableCollection<Service> services)
        {
            var result = 0;

            using (SQLiteConnection cnx = new SQLiteConnection(Const.sqlite_src))
            {
                cnx.Open();
                foreach (var service in services)
                {
                    using (SQLiteCommand cmd = new SQLiteCommand("DELETE FROM service WHERE Id=@0;", cnx))
                    {
                        cmd.Parameters.AddWithValue("0", service.Id);
                        result += cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                cnx.Close();
            }
           
            return result;
        }
        public static async Task<int> RemoveAsync(ObservableCollection<Service> services)
        {
            await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            var result = Remove(services);

            return result;
        }

    }
}
