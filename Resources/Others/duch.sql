-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 05 Avril 2018 à 18:28
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `duch`
--

-- --------------------------------------------------------

--
-- Structure de la table `mark`
--

CREATE TABLE `mark` (
  `id` int(11) NOT NULL,
  `Title` varchar(60) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `mark`
--

INSERT INTO `mark` (`id`, `Title`, `created_at`) VALUES
(1, 'Jeep', '2018-03-28 09:29:15'),
(2, 'Berline', '2018-03-28 09:30:01'),
(3, 'Mini jeep', '2018-03-28 09:43:11'),
(4, 'Lada', '2018-03-28 09:43:11');

-- --------------------------------------------------------

--
-- Structure de la table `point`
--

CREATE TABLE `point` (
  `id` int(11) NOT NULL,
  `worker_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ended_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `point`
--

INSERT INTO `point` (`id`, `worker_id`, `created_at`, `ended_at`) VALUES
(1, 1, '2018-04-05 16:05:49', NULL),
(2, 1, '2018-04-05 18:24:39', NULL),
(3, 1, '2018-04-05 18:24:44', NULL),
(4, 1, '2018-04-05 18:24:46', NULL),
(5, 1, '2018-04-05 18:24:49', NULL),
(6, 1, '2018-04-05 18:24:52', NULL),
(7, 1, '2018-04-05 18:24:55', NULL),
(8, 1, '2018-04-05 18:24:57', NULL),
(9, 1, '2018-04-05 18:25:00', NULL),
(10, 1, '2018-04-05 18:25:03', NULL),
(11, 1, '2018-04-05 18:25:06', NULL),
(12, 1, '2018-04-05 18:25:09', NULL),
(13, 1, '2018-04-05 18:27:52', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `Title` varchar(60) NOT NULL,
  `price` decimal(10,0) DEFAULT '0',
  `mark_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `service`
--

INSERT INTO `service` (`id`, `Title`, `price`, `mark_id`, `created_at`) VALUES
(1, 'Lavage comple', '500', 2, '2018-03-28 09:32:39'),
(2, 'Carrosse sans mousse', '150', 2, '2018-03-28 09:39:17'),
(3, 'Essuyer', '50', 2, '2018-03-28 09:39:17'),
(4, 'Intérieur', '250', 2, '2018-03-28 09:42:03'),
(5, 'Moteur', '350', 2, '2018-03-28 09:42:03'),
(6, 'Coffre', '50', 2, '2018-03-28 09:42:03'),
(7, 'Tapis', '50', 2, '2018-03-28 09:42:03'),
(8, 'Pneus', '50', 2, '2018-03-28 09:42:03'),
(9, 'Aspirateur', '50', 2, '2018-03-28 09:42:34'),
(10, 'Carrosse avec mousse', '200', 2, '2018-03-28 10:12:36'),
(11, 'Lavage comple', '700', 1, '2018-03-28 10:13:36'),
(12, 'Carrosse sans mousse', '250', 1, '2018-03-28 10:13:36'),
(13, 'Carrosse avec mousse', '300', 1, '2018-03-28 10:13:36'),
(14, 'Essuyer', '150', 1, '2018-03-28 10:13:36'),
(15, 'Intérieur', '300', 1, '2018-03-28 10:13:36'),
(16, 'Moteur', '350', 1, '2018-03-28 10:13:36'),
(17, 'Coffre', '50', 1, '2018-03-28 10:13:36'),
(18, 'Tapis', '50', 1, '2018-03-28 10:13:36'),
(19, 'Pneus', '100', 1, '2018-03-28 10:13:36'),
(20, 'Aspirateur', '50', 1, '2018-03-28 10:13:36'),
(21, 'Lavage comple', '600', 3, '2018-03-28 10:13:36'),
(22, 'Carrosse sans mousse', '200', 3, '2018-03-28 10:13:36'),
(23, 'Carrosse avec mousse', '250', 3, '2018-03-28 10:13:36'),
(24, 'Essuyer', '100', 3, '2018-03-28 10:13:36'),
(25, 'Intérieur', '300', 3, '2018-03-28 10:13:36'),
(26, 'Moteur', '350', 3, '2018-03-28 10:13:36'),
(27, 'Coffre', '50', 3, '2018-03-28 10:13:36'),
(28, 'Tapis', '50', 3, '2018-03-28 10:13:36'),
(29, 'Pneus', '100', 3, '2018-03-28 10:13:36'),
(30, 'Aspirateur', '50', 3, '2018-03-28 10:13:36'),
(31, 'Lavage comple', '400', 4, '2018-03-28 10:13:36'),
(32, 'Carrosse sans mousse', '100', 4, '2018-03-28 10:13:36'),
(33, 'Carrosse avec mousse', '150', 4, '2018-03-28 10:13:36'),
(34, 'Essuyer', '50', 4, '2018-03-28 10:13:36'),
(35, 'Intérieur', '200', 4, '2018-03-28 10:13:36'),
(36, 'Moteur', '350', 4, '2018-03-28 10:13:36'),
(37, 'Coffre', '50', 4, '2018-03-28 10:13:36'),
(38, 'Tapis', '50', 4, '2018-03-28 10:13:36'),
(39, 'Pneus', '50', 4, '2018-03-28 10:13:36'),
(40, 'Aspirateur', '50', 4, '2018-03-28 10:13:36');

-- --------------------------------------------------------

--
-- Structure de la table `wash`
--

CREATE TABLE `wash` (
  `id` int(11) NOT NULL,
  `cost` decimal(10,0) DEFAULT '0',
  `mark` varchar(25) NOT NULL,
  `customer` varchar(60) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `remark` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `worker_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `wash`
--

INSERT INTO `wash` (`id`, `cost`, `mark`, `customer`, `mobile`, `remark`, `created_at`, `worker_id`) VALUES
(1, '300', 'Berline', 'Lass Diarra', '+22505895514', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.', '2018-03-31 01:49:22', 1),
(2, '500', 'Berline', 'hhg', '79', 'Pourquoi l\'utiliser?\nOn sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut,', '2018-03-31 02:47:41', 1),
(3, '150', 'Jeep', '', '', NULL, '2018-03-31 11:16:03', 1),
(4, '300', 'Lada', '', '+8989522366', NULL, '2018-03-31 11:37:10', 1),
(5, '650', 'Berline', '', '', NULL, '2018-04-04 23:18:38', 1),
(6, '650', 'Berline', '', '', NULL, '2018-04-04 23:18:39', 1),
(7, '800', 'Jeep', '', '', NULL, '2018-04-05 18:27:02', 1);

-- --------------------------------------------------------

--
-- Structure de la table `wash_services`
--

CREATE TABLE `wash_services` (
  `wash_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `wash_services`
--

INSERT INTO `wash_services` (`wash_id`, `service_id`) VALUES
(1, 3),
(1, 7),
(1, 10),
(2, 1),
(3, 18),
(3, 19),
(4, 33),
(4, 34),
(4, 37),
(4, 40),
(5, 4),
(5, 5),
(5, 6),
(7, 14),
(7, 15),
(7, 16);

-- --------------------------------------------------------

--
-- Structure de la table `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `nom` varchar(60) NOT NULL,
  `sex` varchar(60) NOT NULL,
  `tel` varchar(60) DEFAULT NULL,
  `address` varchar(60) DEFAULT NULL,
  `quota` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `worker`
--

INSERT INTO `worker` (`id`, `nom`, `sex`, `tel`, `address`, `quota`, `created_at`) VALUES
(1, 'Marco syllur', 'M', '+1 1200455454', 'California, USA', 40, '2018-04-05 15:54:14');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `mark`
--
ALTER TABLE `mark`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `point`
--
ALTER TABLE `point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `worker_id` (`worker_id`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mark_id` (`mark_id`);

--
-- Index pour la table `wash`
--
ALTER TABLE `wash`
  ADD PRIMARY KEY (`id`),
  ADD KEY `worker_id` (`worker_id`);

--
-- Index pour la table `wash_services`
--
ALTER TABLE `wash_services`
  ADD PRIMARY KEY (`wash_id`,`service_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Index pour la table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `mark`
--
ALTER TABLE `mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `point`
--
ALTER TABLE `point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `wash`
--
ALTER TABLE `wash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
