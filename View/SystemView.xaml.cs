﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Handler;
using Voze.Helper;
using Voze.Model;
using Voze.ViewModel;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour SettingsView.xaml
    /// </summary>
    public partial class SystemView : UserControl
    {
        private MainWindow main;
        int c = 1;
        private Mark markSelected;
        private int mark_selected_i = -1;
        private ObservableCollection<Service> servicesSelected= new ObservableCollection<Service>();
        public SystemView(MainWindow main)
        {
            this.main = main;
            this.DataContext = new SettingsViewModel();
            InitializeComponent();
            init();
        }

        private void init()
        {
            int i = Configuration.Get();
            if (i == 0) rd_ru.IsChecked = true;
            if (i == 1) rd_fr.IsChecked = true;
            if (i == 2) rd_en.IsChecked = true;
        }

        private void box_c0_0_GotFocus(object sender, RoutedEventArgs e)
        {
            if (box_c0_0.Text.Length == 0)
                txtbox_c0_0.Visibility = Visibility.Collapsed;
        }

        private void box_c0_0_LostFocus(object sender, RoutedEventArgs e)
        {
            if (box_c0_0.Text.Length == 0)
                txtbox_c0_0.Visibility = Visibility.Visible;
        }
        private void box_c0_1_GotFocus(object sender, RoutedEventArgs e)
        {
            if (box_c0_1.Text.Length == 0)
                txtbox_c0_1.Visibility = Visibility.Collapsed;
        }

        private void box_c0_1_LostFocus(object sender, RoutedEventArgs e)
        {
            if (box_c0_1.Text.Length == 0)
                txtbox_c0_1.Visibility = Visibility.Visible;
        }

       

        private void boxmark_GotFocus(object sender, RoutedEventArgs e)
        {
            if (boxmark.Text.Length == 0)
                txtwratermark.Visibility = Visibility.Collapsed;
        }

        private void boxmark_LostFocus(object sender, RoutedEventArgs e)
        {
            if (boxmark.Text.Length == 0)
                txtwratermark.Visibility = Visibility.Visible;
        }

        
        private void addBoxes()
        {
            if (c <= 12)
            {              
                StackPanel panel = new StackPanel();
                panel.Name = "panel" + c;
                panel.Orientation = Orientation.Horizontal;

                TextBox box1 = new TextBox();
                box1.Margin = new Thickness(5);
                box1.Name = "box_c" + c + "_0";
                box1.Width = 230;

                TextBox box2 = new TextBox();
                box2.Margin = new Thickness(5);
                box2.Name = "box_c" + c + "_1";
                box2.Width = 100;

                panel.Children.Add(box1);
                panel.Children.Add(box2);

                aad.Children.Add(panel);
                c++;
            }
        }

        private void removeBoxes()
        {
            boxmark.Clear(); txtwratermark.Visibility = Visibility.Visible;
            box_c0_0.Clear();txtbox_c0_0.Visibility = Visibility.Visible;
            box_c0_1.Clear(); txtbox_c0_1.Visibility = Visibility.Visible;
            if (c > 1)
            {
               
                for (int i = 1; i <= c; i++)
                {
                    StackPanel panel = UIHelper.FindChild<StackPanel>(aad, "panel" + i);
                   aad.Children.Remove(panel); 
                }
            }
        }

        private void save()
        {
            try
            {
               // MessageBox.Show(c + "");
                if (boxmark.Text != "")
                {
                    ObservableCollection<Service> services = new ObservableCollection<Service>();

                    services.Add(new Service()
                    {
                        Title = box_c0_0.Text,
                        Price = Convert.ToDecimal(box_c0_1.Text),
                        MarkId = markSelected.Id
                    });
                    if (c > 1)
                    {
                        for (int i = 1; i < c; i++)
                        {
                            TextBox box1 = UIHelper.FindChild<TextBox>(Application.Current.MainWindow, "box_c" + i + "_0");
                            TextBox box2 = UIHelper.FindChild<TextBox>(Application.Current.MainWindow, "box_c" + i + "_1");
                            if (box1.Text != "")
                            {
                                if (box2.Text == "" || !Formater.IsNumeric(box2.Text))
                                    MsBoxView.Show(Application.Current.FindResource("_addmark").ToString());
                                else
                                    services.Add(new Service()
                                    {
                                        Title = box1.Text,
                                        Price = Convert.ToDecimal(box2.Text),
                                        MarkId = markSelected.Id
                                    });
                            }
                        }
                    }
                    if (services.Count > 0)
                    {
                        int re = ServiceHandler.Add(services);
                        if (re > 0)
                        {
                            int li = lxmark.SelectedIndex;
                            this.DataContext = new SettingsViewModel();
                            removeBoxes();
                            lxmark.SelectedIndex = li;
                            main.reload();
                            c = 1;
                        }
                            
                    }
                    else
                        MsBoxView.Show(Application.Current.FindResource("_addmark").ToString());
                }
                else
                    MsBoxView.Show(Application.Current.FindResource("_addmark").ToString());
            }
            catch (Exception ex)
            {
                Log.i("SystemView: ", ex.ToString());
                MessageBox.Show(ex.Message);
            }
        }

        private void btnaddbox_Click(object sender, RoutedEventArgs e)
        {
            addBoxes();
        }

       

        private void btnsaveserv_Click(object sender, RoutedEventArgs e)
        {
            save();
        }

        private void lxmark_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            markSelected = lxmark.SelectedItem as Mark;
            if (markSelected != null)
            { btnsaveserv.IsEnabled = true; panel_form_serv.Visibility = Visibility.Visible; }
            else
            {
                btnsaveserv.IsEnabled = false; panel_form_serv.Visibility = Visibility.Collapsed;
            }
        }

        private void lxservice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lxservice.SelectedItem != null)
                panel_serv_act.Visibility = Visibility.Visible;
            else
                panel_serv_act.Visibility = Visibility.Collapsed;
        }

        private void btnnewmark_Click(object sender, RoutedEventArgs e)
        {
            var fm = new FormMarkView(Application.Current.FindResource("_addmark").ToString(), 0); fm.ShowDialog();
            if (fm.success)
            {
                this.DataContext = new SettingsViewModel();
                main.reload();
            }
             
        }

        private void btndelservice_Click(object sender, RoutedEventArgs e)
        {
            if (lxservice.SelectedItems.Count > 0)
            {

                servicesSelected.Clear();
                for (int x = 0; x < lxservice.SelectedItems.Count; x++)
                    servicesSelected.Add(lxservice.SelectedItems[x] as Service); 
                int c = ServiceHandler.Remove(servicesSelected);
                if (c > 0)
                {
                    mark_selected_i = lxmark.SelectedIndex;
                    this.DataContext = new SettingsViewModel();
                    lxmark.SelectedIndex = mark_selected_i;
                    main.reload();
                }
                   
                
            }
        }

        private void lxmark_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var data = lxmark.SelectedItem as Mark;
            if (data!=null)
            {
                var result = MsBoxView.Show(Application.Current.FindResource("_confirmation").ToString(),
                   Application.Current.FindResource("_wanttodelete").ToString(),
                   MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    if (MarkHandler.Remove(data) > 0)
                    {
                        main.reload();
                    }
                        this.DataContext = new SettingsViewModel();
                  
                }
            }
        }

      

        private async void changeLang(string ln,int p)
        {
           // int i = this.main.cposi;
            await  Configuration.SetLanguageAsync(ln);
            this.main.lxmenu.ItemsSource = Configuration.getMenu();
            Configuration.Save(p);
            //this.main.lxmenu.SelectedIndex = i;
        }
       
        private void rd_ru_Checked(object sender, RoutedEventArgs e)
        {
            changeLang("ru-RU",0); 
        }

        private void rd_fr_Checked(object sender, RoutedEventArgs e)
        {
            changeLang("fr-FR",1);
        }

        private void rd_en_Checked(object sender, RoutedEventArgs e)
        {
            changeLang("en-US",2);
        }
    }
}
