﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Handler;
using Voze.Model;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour DetailWashView.xaml
    /// </summary>
    public partial class DetailWashView : Window
    {
        private Wash wash;
        public bool succes = false;
        public DetailWashView(Wash wash)
        {
            this.DataContext = this.wash = wash;           
            InitializeComponent();
            //if (wash.Status)
            //    ch_paid.Visibility = Visibility.Collapsed;
        }

        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }


        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
        }

        private void _maximize()
        {
            main.Margin = new Thickness(20);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }


        #endregion

        private void btnaction_Click(object sender, RoutedEventArgs e)
        {
            wash.Remark = tb_comment.Text;
            wash.Status = true;
            succes =  WashHandler.Update(wash) >0 ;
            this.Close();
        }
    }
}
