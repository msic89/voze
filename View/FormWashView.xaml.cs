﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Handler;
using Voze.Model;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour FormWashView.xaml
    /// </summary>
    public partial class FormWashView : Window
    {
        ObservableCollection<Service> services = new ObservableCollection<Service>();
        decimal cost = 0;
        Worker worker;
       public bool success = false;
        public FormWashView(Worker worker)
        {
            this.worker = worker;
            InitializeComponent();
            init();

        }

        private void init()
        {
            try
            {
                lxmark.ItemsSource = MarkHandler.Get();
                cost = 0; services.Clear();
                txcost.Text = "0";
                txcustomer.Clear();
                txmobile.Clear();
                txremark.Clear();
            }
            catch (Exception ex)
            {
                MsBoxView.Show(ex.ToString());
            }
        }

        private void lxmark_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void lxservice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            getSelectedServices();
        }

        void getSelectedServices()
        {
            services.Clear();
            cost = 0;
            if (lxservice.SelectedItems.Count > 0)
            {
                for (int x = 0; x < lxservice.SelectedItems.Count; x++)
                {
                    var srv = lxservice.SelectedItems[x] as Service;
                    services.Add(srv);
                    cost += srv.Price;
                    txcost.Text = cost.ToString();
                }
            }
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            create();
        }

        private  void create()
        {
           
            if (lxmark.SelectedItem != null)
            {
                if (services.Count > 0)
                {
                    var wash = new Wash
                    {
                        Cost = cost,
                        Customer = txcustomer.Text.Trim(),
                        NumCar = tx_carsnumber.Text.Trim(),
                        Mobile = txmobile.Text.Trim(),
                        Remark = txremark.Text.Trim(),
                        Mark = (lxmark.SelectedItem as Mark).Title,
                        WorkerId = worker.Id,
                        Services = services
                    };
                    save.IsEnabled = false;
                    int r =   WashHandler.Add(wash);
                    if (r > 0)
                    {
                        success = true;
                        init();
                        
                    }   
                    this.Close();
                }
                else
                {
                    MsBoxView.Show(Application.Current.FindResource("_select_serv_svp").ToString());
                }

            }
            else { MsBoxView.Show(Application.Current.FindResource("_select_mark_svp").ToString()); }

        }

      

       

        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }

       
        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
        }

        private void _maximize()
        {
            main.Margin = new Thickness(20);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
          private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }
        #endregion
    }
}
