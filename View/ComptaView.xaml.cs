﻿using Voze.Handler;
using Voze.Helper;
using Voze.Model;
using Voze.ViewModel;

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour ComptaView.xaml
    /// </summary>
    public partial class ComptaView : UserControl
    {
        private MainWindow main;

        private Glo.Calendar.Year Year;
        Glo.Calendar.Day day = new Glo.Calendar.Day();
        int year = DateTime.Now.Year;
        int pos = 0;

        public ComptaView(MainWindow main)
        {
            this.main = main;
            InitializeComponent();
            getDataToday();
        }

        public ComptaView()
        {
            InitializeComponent();
            getDataToday();
        }

        private void shorting_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Name == btnshort_date.Name)
            {
                getCustom();pos = 0;
            }
            if (b.Name == btnshort_week.Name)
            {
                getDataLastWeek(); pos = 1;
            }
            if (b.Name == btnshort_month.Name)
            {
                getDataMonth(); pos = 2;
            }
            if (b.Name == btnshort_year.Name)
            {
                getDataYear(); pos = 3;
            }
           
            if (b.Name == btnshort_today.Name)
            {
                getDataToday(); pos = 4;
            }
            if (b.Name == btn_search_by_date.Name)
            {
                getDataByDate();
            }
        }

     
        void goTo(int p)
        {
            switch (p)
            {
                case 0:
                    getDataByDate();
                    break;
                case 1:
                    getDataLastWeek();
                    break;
                case 2:
                    getDataMonth();
                    break;
                case 3:
                    getDataYear();
                    break;
                case 4:
                    getDataToday();
                    break;
            }
        }

        #region filtrer

        private void getCustom()
        {
            btnshort_date.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_today.Background = Brushes.Transparent;
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            bor_filter.HorizontalAlignment = HorizontalAlignment.Left;
            bor_date.Visibility = Visibility.Visible;
            grid_washing.DataContext = null;

        }

       
        private void getDataToday()
        {            
            btnshort_today.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            btnshort_date.Background = Brushes.Transparent;
            bor_filter.HorizontalAlignment = HorizontalAlignment.Center;
            bor_date.Visibility = Visibility.Collapsed;
            grid_washing.DataContext = new ComptaViewModel();
        }

        void getDataLastWeek()
        {
            btnshort_week.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            btnshort_date.Background = Brushes.Transparent;
            bor_filter.HorizontalAlignment = HorizontalAlignment.Center;
            bor_date.Visibility = Visibility.Collapsed;
            //lb_period.Content = "Last week";
            string from = Periode.LastWeekFromToday().ToString("yyyy-MM-dd 00:00:00");
            string to = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
            var data = new ComptaViewModel(from, to);
            grid_washing.DataContext = data;
        }

        private void getDataMonth()
        {
        
            btnshort_month.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_week.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            btnshort_date.Background = Brushes.Transparent;
            bor_filter.HorizontalAlignment = HorizontalAlignment.Center;
            bor_date.Visibility = Visibility.Collapsed;
            //  lb_period.Content = Formater.UCFirst(DateTime.Now.ToString("Y"));
            string from = Periode.FirstDateOfCurrentMonth().ToString("yyyy-MM-dd 00:00:00");
            string to = Periode.LastDateOfCurrentMonth().ToString("yyyy-MM-dd 23:59:59");
            grid_washing.DataContext = new ComptaViewModel(from, to);
           
        }

        void getDataYear()
        {
           
            btnshort_year.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            btnshort_date.Background = Brushes.Transparent;
            bor_filter.HorizontalAlignment = HorizontalAlignment.Center;
            bor_date.Visibility = Visibility.Collapsed;
            // lb_period.Content = DateTime.Now.ToString("yyyy");
            string from = Periode.FirstDateOfCurrentYear().ToString("yyyy-MM-dd 00:00:00");
            string to = Periode.LastDateOfCurrentYear().ToString("yyyy-MM-dd 23:59:59");
            grid_washing.DataContext = new ComptaViewModel(from, to);
          
        }

        private void getDataByDate()
        {
            string d = (cb_day.SelectedItem as Glo.Calendar.Day).D;
            string m = (cb_month.SelectedItem as Glo.Calendar.Month).M;
            string y = cb_year.SelectedValue.ToString();
            
            string from =  Periode.FirstDateOfCurrentYear().ToString(y + "-" + m + "-" + d + " 00:00:00");
            string to = Periode.LastDateOfCurrentYear().ToString(y + "-" + m + "-" + d + " 23:59:59");
            grid_washing.DataContext = new ComptaViewModel(from, to);
        }



        #endregion

        private void dgWash_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DetailWashView fm = new DetailWashView(dgWash.SelectedItem as Wash);
            //fm.DataContext = dgWash.SelectedItem;
            fm.ShowDialog();
        }

        private void windows_Loaded(object sender, RoutedEventArgs e)
        {


            getCalendar();
        }

        private void cb_year_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Year = Glo.Calendar.Calendrier.Get((int)cb_year.SelectedItem);
            bor_date.DataContext = Year;
            int m = DateTime.Now.Month;
            cb_month.SelectedIndex = m - 1;
        }

        private void cb_month_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int d = DateTime.Now.Day;
            cb_day.SelectedIndex = d - 1;
        }

        private void cb_day_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            day = cb_day.SelectedItem as Glo.Calendar.Day;
        }

        #region methods
        private async void getCalendar()
        {
            Year = await Glo.Calendar.Calendrier.GetAsync(2018);
            List<int> ly = new List<int>();
            for (int i = 2018; i < DateTime.Now.Year + 1; i++)ly.Add(i);
            cb_year.ItemsSource = ly;
            cb_year.SelectedIndex = cb_year.Items.Count - 1;
        }

        #endregion

        private void dgAttend_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var worker = dgAttend.SelectedItem as Worker;
           if(worker.Bilan.Win > 0)
            {
                if (worker.Attends.Status < 0)
                {
                    var fm = new PayView(worker);
                    fm.ShowDialog();
                    if (fm.ok)
                        goTo(pos);
                }
                if (worker.Attends.Status > 0)
                {
                    var result = MsBoxView.Show("Confirm", "Did you already pay to cancel ?", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        worker.Attends.Status = 0;
                        if (WorkerHandler.SetStatus(worker.Attends) > 0)
                            goTo(pos);

                    }
                }
            }
        }
    }
}
