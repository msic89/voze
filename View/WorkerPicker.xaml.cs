﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Model;
using Voze.ViewModel;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour WorkerPicker.xaml
    /// </summary>
    public partial class WorkerPicker : Window
    {
        public Worker worker;
        private int order;

        public WorkerPicker(int order=0)
        {
            this.order = order;
            this.DataContext = new WorkerPickerViewModel();
            InitializeComponent();
        }

      

        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }


        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
        }

        private void _maximize()
        {
            main.Margin = new Thickness(20);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }
        #endregion

     

        /*     **********************************************             */
        private void lxworker_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            worker = lxworker.SelectedItem as Worker;
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var fm = new FormWorkerView();
            fm.ShowDialog();
            if (fm.isSaved)
            {
                this.DataContext = new WorkerPickerViewModel();
            }
        }

        private void tx_search_LostFocus(object sender, RoutedEventArgs e)
        {
            // tx_search.Clear();
            tx_search.Visibility = Visibility.Collapsed;
            bor_worker.Visibility = Visibility.Visible;
        }

        private void tx_search_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.DataContext = new WorkerPickerViewModel(tx_search.Text);
        }
       

        private void bsearch_Click(object sender, RoutedEventArgs e)
        {
            tx_search.Focus();
            bor_worker.Visibility = Visibility.Collapsed; tx_search.Visibility = Visibility.Visible;
        }
    }
}
