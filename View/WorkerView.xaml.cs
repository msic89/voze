﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Voze.Handler;
using Voze.Helper;
using Voze.Model;
using Voze.ViewModel;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour WorkerView.xaml
    /// </summary>
    public partial class WorkerView : UserControl
    {


        MainWindow main =null; Worker worker_curent = null;
        public WorkerView(MainWindow main)
        {
            this.main = main;
            InitializeComponent();
            this.DataContext = new WorkerViewModel();
        }

        private void lxworker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lxworker.SelectedIndex > -1)
            {
                worker_curent = lxworker.SelectedItem as Worker;
                if (!grid_worker_infos.IsVisible)
                    grid_worker_infos.Visibility = Visibility.Visible;
                getDataToday();
            }
            
        }


        private void shorting_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Name == btnshort_week.Name)
            {
                getDataLastWeek();
            }
            if (b.Name == btnshort_year.Name)
            {
                getDataYear();
            }
            if (b.Name == btnshort_month.Name)
            {
                getDataMonth();
            }
            if (b.Name == btnshort_today.Name)
            {
                getDataToday();
            }
        }

        #region filtrer
        // Elle 
        private void getDataToday()
        {
            var worker = grid_worker_infos.DataContext as Worker;

            btnshort_today.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            // lb_period.Content = Formater.UCFirst(DateTime.Now.ToString("D"));
            grid_washing.DataContext = new DashboardViewModel(worker);
            txtWin.Text = (Convert.ToDecimal(txtCumul.Text) * Convert.ToInt32(txtQuota.Text) / 100).ToString();

        }

        void getDataLastWeek()
        {
            var worker = grid_worker_infos.DataContext as Worker;
            btnshort_week.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            //lb_period.Content = "Last week";
            string from = Periode.LastWeekFromToday().ToString("yyyy-MM-dd 00:00:00");
            string to = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
            grid_washing.DataContext = new DashboardViewModel(from, to, worker);
            txtWin.Text = (Convert.ToDecimal(txtCumul.Text) * Convert.ToInt32(txtQuota.Text) / 100).ToString();
        }

        private void getDataMonth()
        {
            var worker = grid_worker_infos.DataContext as Worker;
            btnshort_month.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_week.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            //  lb_period.Content = Formater.UCFirst(DateTime.Now.ToString("Y"));
            string from = Periode.FirstDateOfCurrentMonth().ToString("yyyy-MM-dd 00:00:00");
            string to = Periode.LastDateOfCurrentMonth().ToString("yyyy-MM-dd 23:59:59");
            grid_washing.DataContext = new DashboardViewModel(from, to, worker);
            txtWin.Text = (Convert.ToDecimal(txtCumul.Text) * Convert.ToInt32(txtQuota.Text) / 100).ToString();

        }

        void getDataYear()
        {
            var worker = grid_worker_infos.DataContext as Worker;
            btnshort_year.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            // lb_period.Content = DateTime.Now.ToString("yyyy");
            string from = Periode.FirstDateOfCurrentYear().ToString("yyyy-MM-dd 00:00:00");
            string to = Periode.LastDateOfCurrentYear().ToString("yyyy-MM-dd 23:59:59");
            grid_washing.DataContext = new DashboardViewModel(from, to, worker);
            txtWin.Text = (Convert.ToDecimal(txtCumul.Text) * Convert.ToInt32(txtQuota.Text) / 100).ToString();

        }

        #endregion




        private void dgWash_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DetailWashView fm = new DetailWashView(dgWash.SelectedItem as Wash);
            //fm.DataContext = dgWash.SelectedItem;
            fm.ShowDialog();
        }
        
        private void btnaddwash_Click(object sender, RoutedEventArgs e)
        {
            var worker = grid_worker_infos.DataContext as Worker;
            var fm = new FormWashView(worker);
            fm.ShowDialog();
            if (fm.success)
            {
                if (!WorkerHandler.onList(worker.Id))
                    WorkerHandler.AddToList(worker);
                getDataToday();
            }         
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            int i = lxworker.Items.Count; 
            var fm = new FormWorkerView();
            fm.ShowDialog();
            if (fm.isSaved)
            {
                this.DataContext = new WorkerViewModel();
                lxworker.SelectedIndex = i;
            }
                
        }

      

        private void Bedit_Click(object sender, RoutedEventArgs e)
        {
            if(worker_curent != null)
            {
                int i = lxworker.SelectedIndex;
                var f = new FormEditWorkerView(worker_curent);
                f.ShowDialog();
                if (f.ok)
                {
                    this.DataContext = new WorkerViewModel();
                    lxworker.SelectedIndex = i;
                }
            }
        }

        private void Bdelete_Click(object sender, RoutedEventArgs e)
        {
            if(worker_curent != null)
            {
                
                var result = MsBoxView.Show(Application.Current.FindResource("_confirmation").ToString(),
                    Application.Current.FindResource("_wanttodelete").ToString(), 
                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    if (WorkerHandler.Remove(worker_curent) > 0)
                    {
                       // MsBoxView.Show("Succes",worker_curent.Name+ " removed succesfully!", MessageBoxButton.OK, MessageBoxImage.Information);
                        grid_worker_infos.Visibility = Visibility.Hidden;
                        this.DataContext = new WorkerViewModel();
                    }
                }
                
            }
           
        }

        private void bsearch_Click(object sender, RoutedEventArgs e)
        {
            tx_search.Focus();
            bor_worker.Visibility = Visibility.Collapsed;tx_search.Visibility = Visibility.Visible;
        }

        private void tx_search_LostFocus(object sender, RoutedEventArgs e)
        {
           // tx_search.Clear();
            tx_search.Visibility = Visibility.Collapsed;
            bor_worker.Visibility = Visibility.Visible;          
        }

        private void tx_search_TextChanged(object sender, TextChangedEventArgs e)
        {
           
            this.DataContext = new WorkerViewModel(tx_search.Text);
            grid_worker_infos.Visibility = Visibility.Hidden;
        }
    }
}
