﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Handler;
using Voze.Helper;
using Voze.Model;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour FormWorkerView.xaml
    /// </summary>
    public partial class FormWorkerView : Window
    {
        public bool isSaved = false;

        public FormWorkerView()
        {
            InitializeComponent();
        }
        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }


        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
        }

        private void _maximize()
        {
            main.Margin = new Thickness(20);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }
        #endregion

        private int errors()
        {
            int errors = 0;
            if (tx_fname.Text == "")
            {
                tx_fname_error.Text = Application.Current.FindResource("_required_field").ToString();
                tx_fname_error.Visibility = Visibility.Visible;
                errors++;
            }
            else
            {
                tx_fname_error.Text = "";
                tx_fname_error.Visibility = Visibility.Visible;
            }

            if (tx_lname.Text == "")
            {
                tx_lname_error.Text = Application.Current.FindResource("_required_field").ToString();
                tx_lname_error.Visibility = Visibility.Visible;
                errors++;
            }
            else
            {
                tx_lname_error.Text = "";
                tx_lname_error.Visibility = Visibility.Visible;
            }

            if (tx_tel.Text == "")
            {
                tx_tel_error.Text = Application.Current.FindResource("_required_field").ToString();
                tx_tel_error.Visibility = Visibility.Visible;
                errors++;
            }
            else
            {
                tx_percent_error.Text = "";
                tx_percent_error.Visibility = Visibility.Visible;
            }

            if (tx_percent.Text == "" || !Formater.IsUInt(tx_percent.Text))
            {
                tx_percent_error.Text = Application.Current.FindResource("_required_field").ToString();
                tx_percent_error.Visibility = Visibility.Visible;
                errors++;
            }
            else
            {
                tx_percent_error.Text = "";
                tx_percent_error.Visibility = Visibility.Visible;
            }


            return errors;
        }

        private void create()
        {

            if (errors() == 0)
            {
                Worker worker = new Worker
                {

                    Name = tx_lname.Text.Trim() + " " + tx_fname.Text,
                    Sex = EmpGenderChoosen.Text.Trim(),
                    Tel = tx_tel.Text.Trim(),
                    Quota = Convert.ToInt32(tx_percent.Text.Trim()),
                    Address = tx_address.Text,

                };

               isSaved =   WorkerHandler.Add(worker) > 0;

                this.Close();
             }
           
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            create();
        }

        private void CheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            CheckBox chk = sender as CheckBox;
            if (chk.Name == chk_confirm.Name)
            {
                save.IsEnabled = false;
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chk = sender as CheckBox;

            if (chk.Name == chk_confirm.Name)
            {
                save.IsEnabled = true;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            try
            {
                EmpGenderChoosen.Text = rb.Content.ToString();
            }
            catch (Exception)
            {
                EmpGenderChoosen.Text = Application.Current.FindResource("_male").ToString(); ;
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox a = sender as TextBox;

            a.CaretIndex = a.Text.Length;
            a.SelectAll();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void parent_Loaded(object sender, RoutedEventArgs e)
        {
            //Animator.Fade(main, 0.5,1, 0.3, false);
        }
    }
}
