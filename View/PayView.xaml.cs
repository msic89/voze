﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Handler;
using Voze.Helper;
using Voze.Model;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour PayView.xaml
    /// </summary>
    public partial class PayView : Window
    {
        private Worker worker;
        public bool ok = false;

        public PayView(Worker worker)
        {
            worker.CreatedAt = Formater.getSplit(worker.CreatedAt, 0);
            this.DataContext =  this.worker = worker;
            InitializeComponent();
            
        }

        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }


        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
        }

        private void _maximize()
        {
            main.Margin = new Thickness(20);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }


        #endregion

        private void btnaction_Click(object sender, RoutedEventArgs e)
        {
            var net = Convert.ToDecimal(tx_pay_sum.Text);
            var s = Convert.ToDecimal(tb_pay.Text);
            var p = net - s;
            if (p < 0) MsBoxView.Show(Application.Current.FindResource("_sumbigger").ToString());
            else if (p>0)
            {
                var result = MsBoxView.Show(Application.Current.FindResource("_confirmation").ToString(),
                    Application.Current.FindResource("_owehim").ToString()+" " +p+". "+ Application.Current.FindResource("_continue").ToString() + "?", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    worker.Attends.Status = p;
                    save();
                }
                
            }
            else
            {
                worker.Attends.Status = 0;
                save();
            }

        }

        private void save()
        {
            worker.Attends.Finished = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            ok = WorkerHandler.AfterWork(worker.Attends) > 0;
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
