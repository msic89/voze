﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Voze.Handler;
using Voze.Helper;
using Voze.Model;
using Voze.ViewModel;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour DashboardView.xaml
    /// </summary>
    public partial class DashboardView : UserControl
    {
        private MainWindow main;
        int filter = 4;
        public DashboardView(MainWindow main)
        {
            this.main = main;
            InitializeComponent();
            AddHotKeys();
            init();
        }

        private void init()
        {
            this.DataContext = new DashboardViewModel();
            lb_period.Content = Formater.UCFirst(DateTime.Now.ToString("D"));
        }

        private void btnaddtolist_Click(object sender, RoutedEventArgs e)
        {
            if (filter == 4)
            {
                var fm = new WorkerPicker(); fm.ShowDialog();
                if (fm.worker != null)
                {
                    if (!WorkerHandler.onList(fm.worker.Id))
                    {
                        int r = WorkerHandler.AddToList(fm.worker);
                        if (r > 0)
                            init();
                    }
                }

            }
        }

        private void shorting_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Name == btnshort_week.Name)
            {
                filter = 1;
                btnaddtolist.Visibility = Visibility.Collapsed;
                getDataLastWeek();
            }
           
            if (b.Name == btnshort_month.Name)
            {
                filter = 2; btnaddtolist.Visibility = Visibility.Collapsed;
                getDataMonth();
            }
            if (b.Name == btnshort_year.Name)
            {
                filter = 3; btnaddtolist.Visibility = Visibility.Collapsed;
                getDataYear();
            }
            if (b.Name == btnshort_today.Name)
            {
                filter = 4; btnaddtolist.Visibility = Visibility.Visible;
                getDataToday();
            }
        }
        void goTo(int p)
        {
            switch (p)
            {
                
                case 1:
                    getDataLastWeek();
                    break;
                case 2:
                    getDataMonth();
                    break;
                case 3:
                    getDataYear();
                    break;
                case 4:
                    getDataToday();
                    break;
            }
        }


        private void dgWash_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DetailWashView fm = new DetailWashView(dgWash.SelectedItem as Wash);
            //fm.DataContext = dgWash.SelectedItem;
            fm.ShowDialog();
            if (fm.succes)
                goTo(filter);

        }

        #region methods 
        void getDataLastWeek()
        {
            btnshort_week.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            lb_period.Content = "Last week";
            string from = Periode.LastWeekFromToday().ToString("yyyy-MM-dd 00:00:00");
            string to = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
            this.DataContext = new DashboardViewModel(from, to);
        }
        void getDataYear()
        {
            btnshort_year.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            lb_period.Content = DateTime.Now.ToString("yyyy");
            string from = Periode.FirstDateOfCurrentYear().ToString("yyyy-MM-dd 00:00:00");
            string to = Periode.LastDateOfCurrentYear().ToString("yyyy-MM-dd 23:59:59");
            this.DataContext = new DashboardViewModel(from, to);
        }
        private void getDataMonth()
        {
            btnshort_month.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_week.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_today.Background = Brushes.Transparent;
            lb_period.Content = Formater.UCFirst(DateTime.Now.ToString("Y"));
            string from = Periode.FirstDateOfCurrentMonth().ToString("yyyy-MM-dd 00:00:00");
            string to = Periode.LastDateOfCurrentMonth().ToString("yyyy-MM-dd 23:59:59");
            this.DataContext = new DashboardViewModel(from, to);
        }
        private void getDataToday()
        {
            btnshort_today.Background = (Brush)Application.Current.FindResource("colorPrimary");
            btnshort_month.Background = Brushes.Transparent;
            btnshort_year.Background = Brushes.Transparent;
            btnshort_week.Background = Brushes.Transparent;
            lb_period.Content = Formater.UCFirst(DateTime.Now.ToString("D"));
            this.DataContext = new DashboardViewModel();
        } 
        #endregion

        private void lxpointer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (filter == 4)
            {
                var fm = new FormWashView(lxpointer.SelectedItem as Worker); fm.ShowDialog();
                init();
            }        
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddWash_Click(object sender, RoutedEventArgs e)
        {
            addWashing();
        }

        public void addWashing()
        {
            if (filter == 4)
            {
                var fm = new WorkerPicker(); fm.ShowDialog();
                if (fm.worker != null)
                {
                    if (!WorkerHandler.onList(fm.worker.Id))
                    {
                        int r = WorkerHandler.AddToList(fm.worker);
                        if (r > 0)
                        {
                            var wwd = new FormWashView(fm.worker);
                            wwd.ShowDialog();
                            if (wwd.success)
                                init();
                        }

                    }
                    else
                    {
                        var wwd = new FormWashView(fm.worker);
                        wwd.ShowDialog();
                        if (wwd.success)
                            init();
                    }
                }

            }
        }

        #region racourcis
        private void AddHotKeys()
        {
            try
            {
                RoutedCommand rc1 = new RoutedCommand();
                rc1.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(rc1, addWahsing_event_handler));
               
            }
            catch (Exception)
            {
                //handle exception error
            }
        }
        private void addWahsing_event_handler(object sender, RoutedEventArgs e)
        {
            addWashing();
        }
        #endregion  

    }
}
