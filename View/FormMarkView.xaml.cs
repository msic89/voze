﻿using System.Windows;
using System.Windows.Input;
using Voze.Handler;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour Formulaire.xaml
    /// </summary>
    public partial class FormMarkView : Window
    {
        private int order;
        public bool success = false;

        public FormMarkView(string title, int order)
        {
            this.order = order;          
            InitializeComponent();
            txtitle.Text = title;
        }

        private void action()
        {
            switch (order)
            {
                case 0:
                    if (input.Text == "")
                        MsBoxView.Show(Application.Current.FindResource("_required_field").ToString());
                    else {
                        success =  MarkHandler.Insert(input.Text) > 0;
                        this.Close();
                    }
                    break;
            }
        }

        private void btn_win_close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnaction_Click(object sender, RoutedEventArgs e)
        {
            action();
        }

        private void input_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                action();
        }
    }
}
