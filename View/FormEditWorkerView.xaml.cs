﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Voze.Handler;
using Voze.Helper;
using Voze.Model;

namespace Voze.View
{
    /// <summary>
    /// Logique d'interaction pour FormEditWorkerView.xaml
    /// </summary>
    public partial class FormEditWorkerView : Window
    {

        string sex = "";
        public bool ok = false;
        private Worker worker;
        public FormEditWorkerView(Worker worker )
        {
            this.worker = worker;
            InitializeComponent();
            this.DataContext = worker;
            box_lastname.Text = Formater.getSplit(worker.Name, 1);
            box_firstname.Text = Formater.getSplit(worker.Name, 0);
        }

        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }


        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }

        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
        }

        private void _maximize()
        {
            main.Margin = new Thickness(20);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }

        #endregion

       
        private void input_username_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void Gender_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton radio = sender as RadioButton;
            sex = radio.Content.ToString();
        }

        private void bupt_Click(object sender, RoutedEventArgs e)
        {
            if (hasError())
                MsBoxView.Show("Incorrect values of fields");
            else
            {
                this.worker.Name = box_firstname.Text + " " + box_lastname.Text;
                this.worker.Address = box_address.Text;
                this.worker.Tel = box_tel.Text;
                this.worker.Sex = sex == "" ? this.worker.Sex : sex;
                if (Formater.toInt(box_quota.Text) != this.worker.Quota)
                    this.worker.Quota = Formater.toInt(box_quota.Text);

                if (WorkerHandler.Update(worker) > 0)
                    ok = true;
                this.Close();
            }
        }

        private bool hasError()
        {
            if (box_firstname.Text == "" || box_firstname.Text == "" || box_tel.Text == "" || box_quota.Text == "")
                return true;
            else if (!Formater.IsNumeric(box_quota.Text))
                return true;
            return false;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
