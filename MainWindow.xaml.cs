﻿using Voze.View;
using Voze.ViewModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System;
using Voze.Helper;
using Voze.Model;
using Voze.Handler;

namespace Voze
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region attributs
        public int cposi = 0;
        bool istoggled = false;
        bool extended = false;
        bool menu_droped = false;
        private User user;
        private bool canexit = true;
        #endregion

        #region constructors
        public MainWindow()
        {
            // Configuration.SetLanguage();
            this.user = UserHandler.Get(1);
            InitializeComponent();
            AddHotKeys();
            init();
        }
        public MainWindow(User user)
        {
            //  Configuration.SetLanguage();
            this.user = user;
            InitializeComponent();
            AddHotKeys();
            init();
        }
        #endregion



        #region init
        public void init()
        {
            this.DataContext = new MainViewModel(this.user);
            lxmenu.SelectedIndex = 0;
        }

        public void reload(bool ini = false)
        {
            if (ini)
                lxmenu.SelectedIndex = 0;
            this.DataContext = new MainViewModel(this.user);
        }
        #endregion


        #region App bar

        private void resetWindow()
        {
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Maximized;
        }
        private void _minimize()
        {
            this.WindowState = WindowState.Minimized;
        }
        private void _restore()
        {
            // main.BorderBrush = Brushes.Azure;
            main.Margin = new Thickness(0, 50, 50, 50);
            main.BorderThickness = new Thickness(1);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_restore.Visibility = Visibility.Collapsed;
            btn_win_max.Visibility = Visibility.Visible;
            // extend();
        }
        private void _maximize()
        {
            main.Margin = new Thickness(0);
            main.BorderThickness = new Thickness(0);
            this.WindowState = this.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            btn_win_max.Visibility = Visibility.Collapsed;
            btn_win_restore.Visibility = Visibility.Visible;
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == btn_win_min.Name)
                _minimize();

            if (btn.Name == btn_win_max.Name)
            {
                _maximize();
            }

            if (btn.Name == btn_win_restore.Name)
            {
                _restore();
            }

            if (btn.Name == btn_win_close.Name)
                this.Close();
        }


        #endregion

        #region Menu
        private void lxmenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gotoMenu(lxmenu.SelectedIndex);

        }

        private void gotoMenu(int position)
        {

            switch (position)
            {
                case 0:
                    openDashboard();
                    break;
                case 1:
                    openWorkerPanel();
                    break;
                case 2:
                    openAccountingPanel();
                    break;
                case 3:
                    openSystemPanel();
                    break;
                default:

                    break;
            }

            hideMenu();
        }

        private void openSystemPanel()
        {
            cposi = 3;
            wrapper.Children.Clear();
            wrapper.Children.Add(new SystemView(this)); //hideMenu();
        }

        private void openAccountingPanel()
        {
            cposi = 2;
            wrapper.Children.Clear();
            wrapper.Children.Add(new ComptaView(this)); //hideMenu();
        }

        private void openWorkerPanel()
        {
            cposi = 1;
            wrapper.Children.Clear();
            wrapper.Children.Add(new WorkerView(this)); //hideMenu();
        }

        private void openDashboard()
        {
            cposi = 0;
            wrapper.Children.Clear();
            wrapper.Children.Add(new DashboardView(this));
        }

        private void hideMenu()
        {
            if (menu_droped)
            {
                Animator.SlideHeight(bor_dropdown, 0, 0.1); menu_droped = false;
                bor_dropdown.Visibility = Visibility.Collapsed;
            }
        }

        private void showMenu()
        {
            if (!menu_droped)
            {
                Animator.SlideHeight(bor_dropdown, 100, 150, 0.1); menu_droped = true;
                bor_dropdown.Visibility = Visibility.Visible;

            }
        }
        private void btn_dropdown_menu_Click(object sender, RoutedEventArgs e)
        {
            if (menu_droped) { hideMenu(); }
            else { showMenu(); }
        }

        private void bor_dropdown_MouseLeave(object sender, MouseEventArgs e)
        {
            hideMenu();
        }

        private void selectedMenu(int i)
        {
            if (lxmenu.Items.Count > i)
                lxmenu.SelectedIndex = i;
        }
        #endregion

        #region Panels animations
        private void extend2()
        {
            if (extended) { Animator.SlideWidth(panel_left, 300, 0.0); extended = false; /* tno.Visibility = Visibility.Collapsed; */}
            else { Animator.SlideWidth(panel_left, 0, 0.0); extended = true;/* tno.Visibility = Visibility.Visible; */}
        }
        private void extend()
        {
            if (extended) { Animator.SlideToLeft(panel_left, -305, 0.3, true); extended = false; }
            else { Animator.SlideToRight(panel_left, -305, 0.3); extended = true; }
        }
        private void toggle()
        {
            if (istoggled) { Animator.SlideToLeft(panel_right, 300, 0.3, true); istoggled = false; }
            else { Animator.SlideToLeft(panel_right, 300, 0.3); istoggled = true; }
        }

        private void btn_close_right_Click(object sender, RoutedEventArgs e)
        {
            toggle();
        }
        private void btn_close_left_panel_Click(object sender, RoutedEventArgs e)
        {
            extend();
        }
        private void Button_drawer_Click(object sender, RoutedEventArgs e)
        {
            extend();
            // toggle();
        }
        #endregion



        #region   racourcis

        private void parent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                selectedMenu(0); // afficher dashboard
            }
            if (e.Key == Key.F2)
            {
                selectedMenu(1);  // afficher worker panel
            }
            if (e.Key == Key.F3)
            {
                selectedMenu(2);  // afficher accounting panel
            }
            if (e.Key == Key.F4)
            {
                selectedMenu(3); // afficher system panel
            }


        }


        private void AddHotKeys()
        {
            try
            {
                RoutedCommand rc1 = new RoutedCommand();
                rc1.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Alt));
                CommandBindings.Add(new CommandBinding(rc1, Services_event_handler));
                RoutedCommand rc2 = new RoutedCommand();
                rc2.InputGestures.Add(new KeyGesture(Key.P, ModifierKeys.Alt));
                CommandBindings.Add(new CommandBinding(rc2, Show_profil_event_handler));
                RoutedCommand rc3 = new RoutedCommand();
                rc3.InputGestures.Add(new KeyGesture(Key.L, ModifierKeys.Alt));
                CommandBindings.Add(new CommandBinding(rc3, Lock_event_handler));
            }
            catch (Exception)
            {
                //handle exception error
            }
        }
        private void Services_event_handler(object sender, RoutedEventArgs e)
        {
            extend();// afficher services panel
        }
        private void Show_profil_event_handler(object sender, RoutedEventArgs e)
        {
            toggle(); // afficher profile panel
        }
        private void Lock_event_handler(object sender, RoutedEventArgs e)
        {
            verouiller(); // verrouiller window 
        }


        #endregion

        #region Others
        private void btn_logout_Click(object sender, RoutedEventArgs e)
        {

            LoginWindow login = new LoginWindow();
            canexit = false;
            login.Show();
            Configuration.SetSession(0);
            this.Close();

        }

        private void lxservices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // la texte    
        }

        private void parent_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (canexit)
            {
                Configuration.Backup();
                Application.Current.Shutdown();
            }
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            bool error = false;
            bool chpwd = false;
            User usr = new User()
            {
                Id = user.Id,
                Email = txt_email.Text == "" ? user.Email : txt_email.Text,
                UId = user.UId,
                Uname = txt_uname.Text == "" ? user.Uname : txt_uname.Text,
                CreatedAt = user.CreatedAt,
            };
            if (txt_pwd.Password != "" && txt_pwd.Password != txt_rep_pwd.Password)
            {
                error = true;
            }
            else
            {
                chpwd = true;
            }

            if (!error)
            {
                if (chpwd)
                {
                    usr.Pwd = txt_pwd.Password;
                    if (UserHandler.UpdateWithPassword(usr) > 0)
                    {
                        reload();
                        verouiller();
                    }
                }
                else
                {
                    if (UserHandler.Update(usr) > 0)
                    {
                        reload();
                        toggle();
                    }
                }
                txt_pwd.Password = ""; txt_rep_pwd.Password = ""; txt_login_errors.Text = "";
            }
            else
            {
                txt_login_errors.Text = Application.Current.FindResource("_repeatpassword") + "";
            }
        }

        #endregion

        #region login
        private void verouiller()
        {
            grid_lock.Visibility = Visibility.Visible;
            grid_lock2.Visibility = Visibility.Visible;
            tx_lock_pwd.Focus();
        }
        private void deverouiller()
        {
            grid_lock.Visibility = Visibility.Collapsed;
            grid_lock2.Visibility = Visibility.Collapsed;
        }

        private void btn_cancel_login_Click(object sender, RoutedEventArgs e)
        {

            var result = MsBoxView.Show(Application.Current.FindResource("_confirmation").ToString(),
                     Application.Current.FindResource("_wanttoexit").ToString(),
                     MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                canexit = true; this.Close();
            }
        }


        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void chk_remember_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void chk_remember_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void btn_sign_in_Click(object sender, RoutedEventArgs e)
        {
            login();
        }

        private void login()
        {
            if (tx_lock_username.Text != "" && tx_lock_pwd.Password != "")
            {
                var user = UserHandler.Get(tx_lock_username.Text, tx_lock_pwd.Password);
                if (user != null)
                {
                    tx_lock_pwd.Password = "";
                    deverouiller();
                }
                else
                {
                    txt_login_errors.Text = Application.Current.FindResource("_nologin").ToString();
                }
            }
        }

        private void btn_forget_Click(object sender, RoutedEventArgs e)
        {

        }

        private void tx_password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (tx_lock_pwd.Password.Length > 0)
                tx_water_password.Visibility = Visibility.Collapsed;
            else
                tx_water_password.Visibility = Visibility.Visible;
        }

        private void tx_password_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                login();
        }


        #endregion




    }
}
