﻿using Voze.Helper;
using Voze.View;
using System;
using System.Threading;
using System.Windows;

namespace Voze
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Mutex _mutex = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                const string appName = "Voze";
                bool createdNew;

                _mutex = new Mutex(true, appName, out createdNew);

                if (!createdNew)
                {
                    Application.Current.Shutdown();
                }

                Configuration.SetLanguage(Configuration.Get().ToString());
                start();
            }
            catch (Exception )
            {
                MsBoxView.Show("Error"); Application.Current.Shutdown();
            }

            base.OnStartup(e);
        }

        private void start()
        {
            int keep = Configuration.GetSession();
            if (keep > 0)
                this.StartupUri = new System.Uri("MainWindow.xaml", System.UriKind.Relative);
            else
                this.StartupUri = new System.Uri("LoginWindow.xaml", System.UriKind.Relative);
        }
    }
}
