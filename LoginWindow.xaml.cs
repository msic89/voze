﻿using Voze.Handler;
using Voze.Helper;
using Voze.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;

namespace Voze
{
    /// <summary>
    /// Logique d'interaction pour LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private bool canexit=false;
        private bool keepsession = false;
        DispatcherTimer timer = new DispatcherTimer();
        int tick = 0;
        public LoginWindow()
        {          
            InitializeComponent();
            timer.Interval = new TimeSpan(1000);
            timer.Tick += Timer_Tick;
            timer.Start();
        
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            tick++;
            if (tick > 2)
            {
                timer.Stop();
                Animator.SlideToTop(card, -600, 1);
                tick = 0;
            }
                

        }

        #region login

        private void Btn_cancel_login_Click(object sender, RoutedEventArgs e)
        {

            var result = MsBoxView.Show(Application.Current.FindResource("_confirmation").ToString(),
                     Application.Current.FindResource("_wanttoexit").ToString(),
                     MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                canexit = true;
                this.Close();
            }
        }


        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Chk_remember_Checked(object sender, RoutedEventArgs e)
        {
            keepsession = true;
        }

        private void Chk_remember_Unchecked(object sender, RoutedEventArgs e)
        {
            keepsession = false; 
        }

        private void Btn_sign_in_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            if (tx_username.Text != "" && tx_password.Password != "")
            {

                var user = UserHandler.Get(tx_username.Text, tx_password.Password);
                if (user != null)
                {
                    if (keepsession)
                        Configuration.SetSession(1);
                    new MainWindow(user).Show();
                    this.Close();
                }
                else { MsBoxView.Show(Application.Current.FindResource("_nologin").ToString()); }
            }
        }

        private void Btn_forget_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Tx_password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (tx_password.Password.Length > 0)
                tx_water_password.Visibility = Visibility.Collapsed;
            else
                tx_water_password.Visibility = Visibility.Visible;
        }

        private void Tx_password_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                Login();
        }

        #endregion

        private void Parent_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (canexit)
            {
                Configuration.Backup();
                Application.Current.Shutdown();
            }
        }

        private void Parent_Loaded(object sender, RoutedEventArgs e)
        {
          
        }
    }
}
